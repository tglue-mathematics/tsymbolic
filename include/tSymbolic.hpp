/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of the library.

   2019-02-06, Bin Gao:
   * first version
*/

#pragma once

#include "tSymbolic/Settings.hpp"
#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/Number.hpp"
#include "tSymbolic/Vector.hpp"
#include "tSymbolic/SymbolVector.hpp"
#include "tSymbolic/PartialDerivative.hpp"
#include "tSymbolic/Arithmetic.hpp"
#include "tSymbolic/VariableOrder.hpp"
#include "tSymbolic/Substitution.hpp"

#include "tSymbolic/Combinatorics/Numbers.hpp"
#include "tSymbolic/Combinatorics/TwelvefoldWay.hpp"

#include "tSymbolic/Physics/Quantities.hpp"
#include "tSymbolic/Physics/Point.hpp"
#include "tSymbolic/Physics/PhysObject.hpp"

#include "tSymbolic/QuantChem/Operator.hpp"
