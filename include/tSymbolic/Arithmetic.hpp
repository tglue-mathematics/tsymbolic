/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of arithmetic classes.

   2019-02-17, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/Number.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tSymbolic
{

    /* @class:Addition[Addition operation of arithmetic.] */
    class Addition: virtual public Symbol
    {
        public:
            /* @fn:Addition()[Constructor.] */
            explicit Addition(const std::shared_ptr<Symbol> augend,
                              const std::shared_ptr<Symbol> addend) noexcept:
                m_augend(augend),
                m_addend(addend) {}
            /* @fn:~Symbol()[Deconstructor.] */
            virtual ~Addition() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override;
            /* @fn:diffp[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with other given ones.] */
            virtual std::shared_ptr<Symbol> subs(const SymbolSubstitution& substitutions) noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with given std::function's, and each std::function will take the corresponding specified symbol as input.]*/
            virtual std::shared_ptr<Symbol> subs(const FunctionSubstitution& substitutions) noexcept override;
            /* @fn:get_layout[Get the layout of components of the symbol with a given order] */
            virtual std::vector<VariableOrder> get_layout(const unsigned int order) noexcept override;
        private:
            std::shared_ptr<Symbol> m_augend;
            std::shared_ptr<Symbol> m_addend;
    };

    /* Subtraction */

    /* @class:Multiplication[Multiplication operation of arithmetic.] */
    class Multiplication: virtual public Symbol
    {
        public:
            /* @fn:Multiplication()[Constructor.] */
            explicit Multiplication(const std::shared_ptr<Symbol> multiplier,
                              const std::shared_ptr<Symbol> multiplicand) noexcept:
                m_multiplier(multiplier),
                m_multiplicand(multiplicand) {}
            /* @fn:~Symbol()[Deconstructor.] */
            virtual ~Multiplication() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override;
            /* @fn:diffp[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with other given ones.] */
            virtual std::shared_ptr<Symbol> subs(const SymbolSubstitution& substitutions) noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with given std::function's, and each std::function will take the corresponding specified symbol as input.]*/
            virtual std::shared_ptr<Symbol> subs(const FunctionSubstitution& substitutions) noexcept override;
            /* @fn:get_layout[Get the layout of components of the symbol with a given order] */
            virtual std::vector<VariableOrder> get_layout(const unsigned int order) noexcept override;
        private:
            std::shared_ptr<Symbol> m_multiplier;
            std::shared_ptr<Symbol> m_multiplicand;
    };

    /* Division */

    /*Summation*/

    /*Factorial*/

    /* operator + */
    inline std::shared_ptr<Symbol> operator+(const std::shared_ptr<Symbol>& lhs,
                                             const std::shared_ptr<Symbol>& rhs) noexcept
    {
        std::shared_ptr<Symbol> number_zero = std::make_shared<Number>(Number(0));
        if (lhs->equal_to(number_zero)) {
            if (rhs->equal_to(number_zero)) {
                return number_zero;
            }
            else {
                return rhs;
            }
        }
        else {
            if (rhs->equal_to(number_zero)) {
                return lhs;
            }
            else {
                //FIXME: Compare lhs and rhs, if equal, return 2*lhs
                return std::make_shared<Addition>(Addition(lhs, rhs));
            }
        }
    }

    /* operator * */
    inline std::shared_ptr<Symbol> operator*(const std::shared_ptr<Symbol>& lhs,
                                             const std::shared_ptr<Symbol>& rhs) noexcept
    {
        std::shared_ptr<Symbol> number_zero = std::make_shared<Number>(Number(0));
        std::shared_ptr<Symbol> number_one = std::make_shared<Number>(Number(1));
        if (lhs->equal_to(number_zero) || rhs->equal_to(number_zero)) {
            return number_zero;
        }
        else {
            if (lhs->equal_to(number_one)) {
                return rhs;
            }
            else {
                if (rhs->equal_to(number_one)) {
                    return lhs;
                }
                else {
                    //FIXME: Compare lhs and rhs, if equal, return lhs^2
                    return std::make_shared<Multiplication>(Multiplication(lhs, rhs));
                }
            }
        }
    }
}
