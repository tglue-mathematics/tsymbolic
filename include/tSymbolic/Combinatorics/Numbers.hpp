/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of different numbers in combinatorics.

   2020-05-14, Bin Gao:
   * first version
*/

#pragma once

#include <cmath>
#include <limits>
#include <string>
#include <typeinfo>
#include <type_traits>

#include "tGlueCore/Logger.hpp"

#include "tSymbolic/Settings.hpp"

/*@

  @!:[:stem: latexmath]

  @namespace:tSymbolic[Namespace of tSymbolic library.] */
namespace tSymbolic
{
    /* Falling factorial power falling_factorial_power(x, n) x*(x-1)*...*(x-n+1) */

    /* Rising factorial power rising_factorial_power(x, n) x*(x+1)*...*(x+n-1) */

    /* Factorial factorial(n) = falling_factorial_power(n, n) */

    /* Stirling numbers of the first kind (or Stirling cycle numbers) */
    //stirling_cycle_number(n, k)

    /* Stirling number of the second kind (or Stirling partition number),
       denoting the number of ways to partition a set of n elements into k
       subsets */
    //stirling_partition_number(n, k)

    /* Binomial coefficient \binom{n}{k} */
    template<typename T> inline typename std::enable_if<std::is_arithmetic<T>::value, T>::type
    binom_coefficient(const unsigned int n, const unsigned int k) noexcept
    {
        if (k>n) {
            return static_cast<T>(0);
        }
        else if (k==0 || k==n) {
            return static_cast<T>(1);
        }
        else if (k==1 || k==n-1) {
            return static_cast<T>(n);
        }
        else {
            /* We use floating-point number for calculation */
            long double n_choose_k = 1;
            unsigned int n_plus_one = n+1;
            if (k<n-k) {
                for (unsigned int inum=n; inum>n-k; inum--) {
                    n_choose_k = (n_choose_k*inum)/(n_plus_one-inum);
                }
            }
            else {
                for (unsigned int inum=n; inum>k; inum--) {
                    n_choose_k = (n_choose_k*inum)/(n_plus_one-inum);
                }
            }
            if (n_choose_k>std::numeric_limits<T>::max() ||
                n_choose_k<std::numeric_limits<T>::min()) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "tSymbolic::binom_coefficient<",
                                        typeid(T).name(),
                                        ">() is given with n=",
                                        n,
                                        ", and k=",
                                        k,
                                        ", and result is ",
                                        n_choose_k,
                                        " with overflow error when converting the result");
                return std::numeric_limits<T>::max();
            }
            else {
                return static_cast<T>(nearbyint(n_choose_k));
            }
        }
    }

    /* Number of partitions of n into k parts number_partitions(n, k) */

    /* Rank a given k-combination (zero-based indexing) according to the
       combinatorial number system, see for example
       http://en.wikipedia.org/wiki/Combinatorial_number_system

       @param:beginCombin[sorted in ascending order] */
    template<typename T>
    inline typename std::enable_if<std::is_integral<typename std::iterator_traits<T>::value_type>::value, unsigned int>::type
    rank_combination(const unsigned int k, T beginCombin) noexcept
    {
        using RankType = unsigned int;
        RankType rank = 0;
        for (unsigned int knum=1; knum<=k; ++knum,++beginCombin) {
            auto binom_k = binom_coefficient<RankType>(*beginCombin, knum);
            if (binom_k==std::numeric_limits<RankType>::max()) {
                Settings::logger->write(tGlueCore::MessageType::Error,
                                        "tSymbolic::rank_combination<",
                                        typeid(T).name(),
                                        ">() called for ",
                                        k,
                                        "-combination");
                return std::numeric_limits<RankType>::max();
            }
            rank += binom_k;
        }
        return rank;
    }

    /* Rank a given k-combination (zero-based indexing) according to the
       combinatorial number system, see for example
       http://en.wikipedia.org/wiki/Combinatorial_number_system

       @param:x[Combination in an integer representation] */
    template<typename BitType=unsigned int>
    inline typename std::enable_if<std::is_integral<BitType>::value, unsigned int>::type
    rank_combination(const unsigned int k, BitType x) noexcept
    {
        using RankType = unsigned int;
        RankType rank = 0;
        /* Loop over all bits of the integer representation x */
        BitType chosen_bit = 1;
        for (unsigned int ibit=0,knum=1; ibit<std::numeric_limits<BitType>::digits && knum<=k; ++ibit) {
            /* Check if the current bit is 1 */
            if ((x & chosen_bit)!=0) {
                auto binom_k = binom_coefficient<RankType>(ibit, knum);
                if (binom_k==std::numeric_limits<RankType>::max()) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "tSymbolic::rank_combination<",
                                            typeid(BitType).name(),
                                            ">() called for ",
                                            k,
                                            "-combination");
                    return std::numeric_limits<RankType>::max();
                }
                rank += binom_k;
                ++knum;
            }
            chosen_bit <<= 1;
        }
        return rank;
    }
}
