/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of twelvefold way in combinatorics (see for
   example https://en.wikipedia.org/wiki/Twelvefold_way).

   2020-05-21, Bin Gao:
   * first version
*/

#pragma once

#include <limits>
#include <vector>

#include "tGlueCore/Logger.hpp"

#include "tSymbolic/Settings.hpp"

/*@

  @!:[:stem: latexmath]

  @namespace:tSymbolic[Namespace of tSymbolic library.] */
namespace tSymbolic
{
    /* 3. Counting subsets with k elements of N (also called k-combinations of N)

          Modified from Gosper's hack, and generated combinations strictly
          follow the lexicographic order, so that their ranks are in ascending
          order in the combinatorial number system of degree k

          The combination can be got from for_each_combination() and
          make_combination() */
    template<typename BitType=unsigned int, typename Callable>
    inline bool for_each_combination(const unsigned int n, const unsigned int k, Callable f) noexcept
    {
        /* Get the number of combinations */
        using CombinType = unsigned int;
        auto num_combinations = binom_coefficient<CombinType>(n, k);
        if (num_combinations==std::numeric_limits<CombinType>::max()) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "tSymbolic::for_each_combination() called with number n ",
                                    n,
                                    " and number k ",
                                    k);
            return false;
        }
        /* The first integer is 0...01...1, where the rightmost k bits are 1 */
        BitType x = 1;
        x <<= k;
        --x;
        f(x);
        for (CombinType icomb=1; icomb<num_combinations; ++icomb) {
            /* Extract rightmost bit 1 */
            BitType u = x & -x;
            /* Set last non-trailing bit 0, and clear to the right; unlike the
               original Gosper's hack
               (http://en.wikipedia.org/wiki/Combinatorial_number_system), we do
               not detect the overflow or the case of x==0 by checking if v==0, as
               the number of combinations was already computed */
            BitType v = u + x;
            /* Add lost bit 1's (((v^x)/u)>>2) to the right */
            x = v + (((v^x)/u)>>2);
            f(x);
        }
        return true;
    }

    /* Make a k-combination from a given integer representation */
    template<typename T, typename BitType=unsigned int>
    inline std::vector<T> make_combination(const BitType x,
                                           const unsigned int k,
                                           const std::vector<T>& elements) noexcept
    {
        std::vector<T> combination;
        combination.reserve(k);
        /* Loop over all bits of the integer representation x */
        BitType chosen_bit = 1;
        for (unsigned int ielm=0,knum=0; ielm<elements.size() && knum<k; ++ielm) {
            /* Check if the current bit is 1 */
            if ((x & chosen_bit)!=0) {
                combination.push_back(elements[ielm]);
                ++knum;
            }
            chosen_bit <<= 1;
        }
        return combination;
    }

    /* 4. Counting multisets with n elements from K (also called
          n-multicombinations)

          The problem can be solved by listing all possible ways to distribute
          n indistinguishable balls into k boxes, and the number of all
          possible ways is binom_coefficient(n+k-1, n)

          Modified from the next composition of n into k parts (NEXCOM) algorithm */
    template<typename Callable>
    inline void for_each_multiset(const unsigned int n, const unsigned int k, Callable f) noexcept
    {
        /* Record numbers of balls in each box */
        std::vector<unsigned int> boxes;
        /* The first multiset is to put all n balls into the first box */
        boxes.assign(k, 0);
        boxes[0] = n;
        f(boxes);
        /* First box with non-zero balls */
        int first_nnz_box = -1;
        /* Number of balls in the first box with non-zero balls */
        unsigned int non_zero_balls = n;
        while (boxes.back()!=n) {
            if (non_zero_balls>1) first_nnz_box = -1;
            ++first_nnz_box;
            /* NEXKSB algorithm to find the next multiset */
            non_zero_balls = boxes[first_nnz_box];
            boxes[first_nnz_box] = 0;
            boxes[0] = non_zero_balls-1;
            ++boxes[first_nnz_box+1];
            f(boxes);
        }
    }

    /* 5. Counting multisets with n elements from K, for which each element of
          K occurs at least once, equivalent to counting the compositions of n
          with k (non-zero) terms, by listing the multiplicities of the elements
          of k in order

          This problem is implemented by using for_each_combination() and
          make_strict_composition() */
    template<typename T, typename BitType=unsigned int>
    inline typename std::enable_if<std::is_integral<typename std::iterator_traits<T>::value_type>::value, void>::type
    make_strict_composition(const BitType x, const unsigned int n, const unsigned int k, T beginComp) noexcept
    {
        /* Loop over all bits of the integer representation x */
        BitType chosen_bit = 1;
        unsigned int offset = 0;
        for (unsigned int ielm=1,knum=1; ielm<n && knum<k; ++ielm) {
            /* Check if the current bit is 1 */
            if ((x & chosen_bit)!=0) {
                *beginComp = ielm-offset;
                offset = ielm;
                ++beginComp;
                ++knum;
            }
            chosen_bit <<= 1;
        }
        *beginComp = n-offset;
    }
}
