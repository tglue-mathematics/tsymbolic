/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of matrix class.

   2020-04-22, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "tSymbolic/Symbol.hpp"

namespace tSymbolic
{
    /*@ == Matrix

        Many properties can be calculated as derivatives of an energy
        function with respect to specified physical variables. */
    class Matrix: virtual public Symbol
    {
        public:
            explicit Matrix(const unsigned int numRows,
                            const unsigned int numColumns,
                            const std::vector<std::shared_ptr<Symbol>> elements) noexcept:
                m_num_rows(numRows),
                m_num_cols(numColumns),
                m_elements(elements) {}
            virtual ~Matrix() noexcept = default;
            virtual std::string to_string() const noexcept override;
            /* @fn:diffp[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override;
            /* @fn:get_layout[Get the layout of components of the symbol with a given order] */
            virtual std::vector<VariableOrder> get_layout(const unsigned int order) noexcept override;
            ///* Get the size (number of components) of the variable for a given rank */
            //virtual unsigned long int size(const unsigned int rank) noexcept = 0;
        private:
            unsigned int m_num_rows;
            unsigned int m_num_cols;
            std::vector<std::shared_ptr<Symbol>> m_elements;
    };
}
