/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of number class.

   2019-02-06, Bin Gao:
   * first version
*/

#pragma once

#include <complex>
#include <memory>
#include <string>
#include <typeindex>

#include "tSymbolic/Symbol.hpp"

/*@

  @!:[:stem: latexmath]

  @namespace:tSymbolic[Namespace of tSymbolic library.] */
namespace tSymbolic
{
    class Number: virtual public Symbol
    {
        public:
            /* @fn:Number()[Constructor.] */
            explicit Number(const int value) noexcept:
                m_container(value),
                m_val_type(typeid(int)) {}
            explicit Number(const double value) noexcept:
                m_container(value),
                m_val_type(typeid(double)) {}
            explicit Number(const std::complex<double> value) noexcept:
                m_container(value),
                m_val_type(typeid(std::complex<double>)) {}
            /* @fn:~Number()[Deconstructor.] */
            virtual ~Number() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override;
            /* @fn:diffp[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override;
        private:
            /*FIXME: may change to std::variant */
            union container
            {
                container(const int value): integer(value) {}
                container(const double value): real_number(value) {}
                container(const std::complex<double> value): complex_number(value) {}
                int integer;
                double real_number;
                std::complex<double> complex_number;
            } m_container;
            std::type_index m_val_type;
    };
}
