/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of partial derivative class.

   2019-09-30, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "tSymbolic/Symbol.hpp"

namespace tSymbolic
{
    /* std::vector<std::pair<tSymbol,unsigned int>>, commutative */
    /* https://en.wikipedia.org/wiki/Product_rule */
    class PartialDerivative: virtual public Symbol
    {
        public:
            explicit PartialDerivative(const std::shared_ptr<Symbol> fun, const VariableOrder var) noexcept:
                m_fun(fun),
                m_var(var) {}
            virtual ~PartialDerivative() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override;
            /* @fn:diffp[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with other given ones.] */
            virtual std::shared_ptr<Symbol> subs(const SymbolSubstitution& substitutions) noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with given std::function's, and each std::function will take the corresponding specified symbol as input.]*/
            virtual std::shared_ptr<Symbol> subs(const FunctionSubstitution& substitutions) noexcept override;
            /* @fn:get_layout[Get the layout of components of the symbol with a given order] */
            virtual std::vector<VariableOrder> get_layout(const unsigned int order) noexcept override;
        private:
            std::shared_ptr<Symbol> m_fun;
            VariableOrder m_var;
    };
}
