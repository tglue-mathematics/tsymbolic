/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of physical object.

   2020-05-21, Bin Gao:
   * first version
*/

#pragma once

#include <algorithm>
#include <complex>
#include <memory>
#include <string>

#include "tGlueCore/Stringify.hpp"

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/SymbolVector.hpp"
#include "tSymbolic/Vector.hpp"
#include "tSymbolic/VariableOrder.hpp"
#include "tSymbolic/Physics/Point.hpp"
#include "tSymbolic/Combinatorics/Numbers.hpp"
#include "tSymbolic/Combinatorics/TwelvefoldWay.hpp"

namespace tSymbolic
{
    /* @class:PhysObject[Physical object.] */
    template<typename PointType=SymbolVector<Point,Vector>>
    class PhysObject: virtual public Symbol
    {
        public:
            /* @fn:Point()[Constructor.]
               @param[in]:minIndex[Minimum index of points.]
               @param[in]:maxIndex[Maximum index of points.]
               @param[in]:minNumPoints[Minimum number of points for a layout.]
               @param[in]:maxNumPoints[Maximum number of points for a layout.]
               @param[in]:frequency[Frequency of the geometrical perturbation.] */
            explicit PhysObject(const std::vector<std::shared_ptr<PointType>> points,
                                const unsigned int minNumPoints=1,
                                const unsigned int maxNumPoints=4,
                                const std::complex<double> frequency=std::complex<double>(0,0)) noexcept:
                m_points(points),
                m_frequency(frequency)
            {
                unsigned int num_points = points.size();
                m_min_npoints = minNumPoints>num_points ? num_points : minNumPoints;
                m_max_npoints = maxNumPoints>num_points ? num_points : maxNumPoints;
            }
            virtual ~PhysObject() noexcept = default;
            //virtual std::vector<unsigned int> expand_components(const unsigned int) noexcept
            //{
            //    //mapping between higher order and product of lower order
            //    //positions of derivatives with respect to components
            //    //
            //    //bra(2), Op(1), ket(4); order(6), center(4)
            //    //bra(2)^6, Op(1)^0, ket(4)^0; --> rank
            //    //...,
            //    //bra(2)^0, Op(1)^0, ket(4)^6
            //    return std::vector<unsigned int>();
            //}
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override
            {
                return std::make_shared<PhysObject<PointType>>(std::move(*this));
            }
            virtual std::string to_string() const noexcept override
            {
                return "{physical-object: {minimum-layout-points: "+std::to_string(m_min_npoints)
                    +", maximum-layout-points: "+std::to_string(m_max_npoints)
                    +", frequency: ("+std::to_string(m_frequency.real())+','+std::to_string(m_frequency.imag())+"i)"
                    +", points: "+tGlueCore::stringify(m_points.cbegin(), m_points.cend())+"}}";
            }
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override
            {
                if (other->type_id()==type_id()) {
                    /*FIXME: If pattern match works, we will remove dynamic cast */
                    std::shared_ptr<PhysObject<PointType>> other_cast
                        = std::dynamic_pointer_cast<PhysObject<PointType>>(other);
                    if (m_min_npoints==other_cast->m_min_npoints &&
                        m_max_npoints==other_cast->m_max_npoints &&
                        m_frequency==other_cast->m_frequency &&
                        m_points.size()==other_cast->m_points.size()) {
                        auto each_point = m_points.cbegin();
                        auto other_point = other_cast->m_points.cbegin();
                        for (; each_point!=m_points.cend(); ++each_point,++other_point) {
                            if (!(*each_point)->equal_to(*other_point)) return false;
                        }
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override
            {
                return m_points.front()->max_diff_order(var);
            }
            /* @fn:diffp[Take partial differentiation.]
               var contains unique variable and non-zero order */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override
            {
                return nullptr;
            }
            /* @fn:subs[Return a new symbol by substituting specified symbols with other given ones.] */
            virtual std::shared_ptr<Symbol> subs(const SymbolSubstitution& substitutions) noexcept override
            {
                return nullptr;
            }
            /* @fn:subs[Return a new symbol by substituting specified symbols with given std::function's, and each std::function will take the corresponding specified symbol as input.]*/
            virtual std::shared_ptr<Symbol> subs(const FunctionSubstitution& substitutions) noexcept override
            {
                return nullptr;
            }
            /* @fn:get_layout[Get the layout of components of the symbol with a given order.] */
            virtual std::vector<VariableOrder> get_layout(const unsigned int order) noexcept override
            {
                if (order==0) return std::vector<VariableOrder>();
                /* Only one point */
                if (m_points.size()==1) return m_points[0]->get_layout(order);
                /* One point layout */
                std::vector<VariableOrder> comp_layout;
                if (m_min_npoints==1) {
                    for (auto& each_point: m_points) {
                        auto point_layout = each_point->get_layout(order);
                        comp_layout.insert(comp_layout.end(), point_layout.begin(), point_layout.end());
                    }
                }
                if (order==1) return comp_layout;
                /* Layout with more than 1 point */
                using BitType = unsigned int;
                unsigned int num_points = m_points.size();
                for (unsigned int layout_npoints=std::max<unsigned int>(2, m_min_npoints);
                     layout_npoints<=std::min(m_max_npoints, order);
                     ++layout_npoints) {
                    /* Get all order compositions */
                    std::vector<std::vector<unsigned int>> order_compositions;
                    for_each_combination<BitType>(order-1, layout_npoints-1, [&](const BitType& x) {
                        order_compositions.push_back(std::vector<unsigned int>(layout_npoints));
                        tSymbolic::make_strict_composition(
                            x, order, layout_npoints, order_compositions.back().begin()
                        );
                    });
                    /* Choose \var(layout_npoints) points */
                    for_each_combination<BitType>(num_points, layout_npoints, [&](const BitType& x) {
                        auto chosen_points = tSymbolic::make_combination<std::shared_ptr<PointType>>(
                            x, layout_npoints, m_points
                        );
                        /* For each set of chosen points, loop all order compositions */
                        for (auto const& point_orders: order_compositions) {
                            /* Get the layout of the first point */
                            auto points_layout = chosen_points[0]->get_layout(point_orders[0]);
                            /* Get the layout of other points */
                            for (unsigned int ipt=1; ipt<layout_npoints; ++ipt) {
                                /* The layout of chosen points is the direct
                                   product of each point's layout */
                                tSymbolic::direct_product(points_layout,
                                                          chosen_points[ipt]->get_layout(point_orders[ipt]));
                            }
                            comp_layout.insert(comp_layout.end(), points_layout.begin(), points_layout.end());
                        }
                    });
                }
                return comp_layout;
            }

            virtual bool has_point(const std::shared_ptr<PointType>& point) const noexcept
            {
                for (auto const& each_point: m_points) {
                    if (each_point->equal_to(point)) return true;
                }
                return false;
            }

            /* fn:get_number_layouts[Get the number of layouts.] */
            inline unsigned int get_number_layouts(const unsigned int order) const noexcept
            {
                using SizeType = unsigned int;
                unsigned int num_points = m_points.size();
                SizeType number_layouts = 0;
                /* Layout with different numbers of points */
                for (unsigned int layout_npoints=m_min_npoints; layout_npoints<=m_max_npoints; ++layout_npoints) {
                    /* Number of combinations for chosen number of points \binom{num_points}{layout_npoints} */
                    auto num_center_combin = binom_coefficient<SizeType>(num_points, layout_npoints);
                    if (num_center_combin==std::numeric_limits<SizeType>::max()) {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "PhysObject::() could not handle the number of differentiated centers");
                        return std::numeric_limits<SizeType>::max();
                    }
                    /* Number of order compositions \binom{order-1}{layout_npoints-1} */
                    auto num_order_combin = binom_coefficient<SizeType>(order-1, layout_npoints-1);
                    if (num_order_combin==std::numeric_limits<SizeType>::max()) {
                        Settings::logger->write(tGlueCore::MessageType::Error,
                                                "PhysObject::() could not handle the given order and number of differentiated centers");
                        return std::numeric_limits<SizeType>::max();
                    }
                    number_layouts += num_center_combin*num_order_combin;
                }
                return number_layouts;
            }
        private:
            unsigned int m_min_npoints;
            unsigned int m_max_npoints;
            std::complex<double> m_frequency;
            std::vector<std::shared_ptr<PointType>> m_points;
    };
}
