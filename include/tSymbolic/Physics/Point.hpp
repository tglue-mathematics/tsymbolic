/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of point class.

   2020-05-21, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>

#include "tSymbolic/Symbol.hpp"

namespace tSymbolic
{
    /* @class:Point[Point that can be used to construct physical object.] */
    class Point: virtual public Symbol
    {
        public:
            /* @fn:Point()[Constructor.] */
            explicit Point(const int index) noexcept: m_index(index) {}
            /* @fn:~Point()[Deconstructor.] */
            virtual ~Point() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
            //virtual bool equal_to(const std::shared_ptr<Point>& other) const noexcept;
        private:
            int m_index;
            //std::array<,3> m_coord;
    };
}
