/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of physical quantities.

   2020-05-15, Bin Gao:
   * first version
*/

#pragma once

#include <complex>
#include <memory>
#include <string>

#include "tSymbolic/Symbol.hpp"

namespace tSymbolic
{
    /* @class:MagFluxDensity[Magnetic flux density B.] */
    class MagFluxDensity: virtual public Symbol
    {
        public:
            /* @fn:MagFluxDensity()[Constructor.]
               @param[in]:frequency[Frequency of the magnetic field.] */
            explicit MagFluxDensity(const std::complex<double> frequency=std::complex<double>(0.0, 0.0)) noexcept:
                m_frequency(frequency) {}
            /* @fn~MagFluxDensity()[Deconstructor.] */
            virtual ~MagFluxDensity() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
        private:
            std::complex<double> m_frequency;
    };

    /* @class:TotalRotMomentum[Total rotational angular momentum.] */
    class TotalRotMomentum: virtual public Symbol
    {
        public:
            /* @fn:TotalRotMomentum()[Constructor.]
               @param[in]:frequency[Frequency of the total rotational angular momentum.] */
            explicit TotalRotMomentum(const std::complex<double> frequency=std::complex<double>(0.0, 0.0)) noexcept:
                m_frequency(frequency) {}
            /* @fn:~TotalRotMomentum()[Deconstructor.] */
            virtual ~TotalRotMomentum() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
        private:
            std::complex<double> m_frequency;
    };
}
