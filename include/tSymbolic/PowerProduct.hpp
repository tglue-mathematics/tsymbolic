/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements a class containing a vector of variables and their
   orders or exponents.

   2020-05-04, Bin Gao:
   * first version
*/

#pragma once

#include <iterator>
#include <memory>
#include <string>
#include <typeindex>
#include <utility>
#include <vector>

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/Number.hpp"
#include "tSymbolic/VariableOrder.hpp"

/*@

  @!:[:stem: latexmath]

  @namespace:tSymbolic[Namespace of tSymbolic library.] */
namespace tSymbolic
{
    /* Positive integer exponents of variables (used for monomial), or positive
       integer orders of variables (used for partial differentiation) */
    class PowerProduct: virtual public Symbol
                        //public std::enable_shared_from_this<PowerProduct>
    {
        public:
            /* Creates a smart pointer instance of the symbol */
            inline static std::shared_ptr<Symbol> create(const VariableOrder varOrders) noexcept
            {
                auto new_symbol = std::shared_ptr<Symbol>(new(std::nothrow) PowerProduct(varOrders));
                if (!new_symbol) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "PowerProduct::create() could not allocate storage space for the variables ",
                                            stringify(varOrders));
                }
                return new_symbol;
            }
            virtual ~PowerProduct() noexcept = default;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
            /* @fn:diffp[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) const noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with other given ones.] */
            virtual std::shared_ptr<Symbol> subs(const SymbolSubstitution& substitutions) const noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with given std::function's, and each std::function will take the corresponding specified symbol as input.]*/
            virtual std::shared_ptr<Symbol> subs(const FunctionSubstitution& substitutions) const noexcept override;
            /* @fn:type_id[Get the runtime type of an addition.]
               @return:[Runtime type of the addition.] */
            virtual std::type_index type_id() const noexcept override;
            /* Get varaible orders */
            inline VariableOrder get_var_orders() const noexcept
            {
                return m_var_orders;
            }
            /* Get the number of variable component orders */
            inline unsigned int get_size_component() const noexcept
            {
                unsigned int size_component = 1;
                for (auto const& each_component: m_comp_orders) {
                    size_component *= each_component.size();
                }
                return size_component;
            }
            /* Move each iterator to the corresponding first component of the aggregate */
            inline void first_component_orders() noexcept
            {
                auto each_component = m_comp_orders.begin();
                for (auto& each_iter: m_iter_component) {
                    each_iter = each_component->begin();
                    ++each_component;
                }
            }
            /* Get the current variable component orders */
            inline VariableOrder get_component_orders() const noexcept
            {
                VariableOrder component_orders;
                for (auto const& each_iter: m_iter_component) {
                    component_orders.push_back(*each_iter);
                }
                return component_orders;
            }
            /* Move to the next variable component orders */
            inline void next_component_orders() noexcept
            {
                auto each_component = m_comp_orders.begin();
                for (auto& each_iter: m_iter_component) {
                    /* Move to next component orders of the current aggregate */
                    ++each_iter;
                    if (each_iter==each_component->end()) {
                        /* Reset to the first component orders once we reach
                           the end of the current aggregate */
                        each_iter = each_component->begin();
                    }
                    else {
                        /* Next variable component orders got, we can exit
                           the loop of iterators */
                        break;
                    }
                    ++each_component;
                }
            }
        protected:
            explicit PowerProduct(const VariableOrder varOrders) noexcept:
                m_var_orders(varOrders)
            {
                /* Make the layout of each aggregate variable (e.g. vector or matrix) */
                m_comp_orders.clear();
                m_iter_component.clear();
                for (auto const& each_aggregate: m_var_orders) {
                    m_comp_orders.push_back(each_aggregate.first->get_layout(each_aggregate.second));
                    m_iter_component.push_back(m_comp_orders.back().begin());
                }
            }
            PowerProduct(const PowerProduct& other) noexcept = default;
        private:
            VariableOrder m_var_orders;
            std::vector<VariableOrder> m_comp_orders;
            std::vector<VariableOrder::iterator> m_iter_component;
    };
}
