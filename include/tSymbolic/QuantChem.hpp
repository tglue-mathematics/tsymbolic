/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of quantum chemistry.

   2019-09-30, Bin Gao:
   * first version
*/

#pragma once

#include "tSymbolic/QuantChem/PhysicalVariable.hpp"

#include "tSymbolic/QuantChem/BasisSet.hpp"

#include "tSymbolic/QuantChem/QuantumOperator.hpp"

#include "tSymbolic/QuantChem/OneElecRepresentation.hpp"
