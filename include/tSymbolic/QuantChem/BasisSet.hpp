/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file defines basis function and basis set classes.

   2020-05-03, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <vector>

#include "tSymbolic/Symbol.hpp"

namespace tSymbolic
{
    //atomic orbital (HF, DFT, non-zero derivatives returned), slater
    //determinant (coupled cluster, zero derivatives returned)
    class BasisFunction: virtual public tSymbolic::Symbol
    {
        public:
            explicit BasisFunction() noexcept = default;
            virtual ~BasisFunction() noexcept = default;
    };

    /* A container of BasisFunction */
    class BasisSet
    {
        public:
            explicit BasisSet() noexcept = default;
            virtual ~BasisSet() noexcept = default;
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:variable[The given (physical) variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<tSymbolic::Symbol>& var) const noexcept = 0;
            /* @fn:get_basis_function[Get basis functions.] */
            virtual std::vector<std::shared_ptr<BasisFunction>> get_basis_function() const noexcept = 0;
    };
}
