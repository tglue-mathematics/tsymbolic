/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of integrator class for different quantum
   operators.

   2020-05-03, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <typeindex>

#include "tGlueCore/Logger.hpp"

#include "tSymbolic/Settings.hpp"
#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/QuantChem/BasisSet.hpp"
#include "tSymbolic/QuantChem/Operator.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tSymbolic
{
    /* == Integrator class for quantum operators

       @class:OneElecIntegrator[Integrator class for one-electron operators.] */
    class OneElecIntegrator
    {
        public:
            explicit OneElecIntegrator() noexcept = default;
            virtual ~OneElecIntegrator() noexcept = default;
            /* integrals <= alpha*diffp(<bra|oper|ket>) + beta*integrals,
               when beta==0, integrals <= alpha*diffp(<bra|oper|ket>),
               for instance h <= G + h */
            virtual bool eval(const std::shared_ptr<tSymbolic::Number>& alpha,
                              const std::shared_ptr<OneElecOper>& oper,
                              const std::shared_ptr<tSymbolic::Number>& beta,
                              std::vector<std::shared_ptr<tSymbolic::Matrix>>& integrals) noexcept
            {
                auto number_zero = tSymbolic::Number::create(0);
                if (!number_zero) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "OneElecIntegrator::eval() could not allocate storage space for number 0");
                    return false;
                }
                auto number_one = tSymbolic::Number::create(1);
                if (!number_one) {
                    Settings::logger->write(tGlueCore::MessageType::Error,
                                            "OneElecIntegrator::eval() could not allocate storage space for number 1");
                    return false;
                }
                if (alpha->equal_to(number_zero)) {
                    /* integrals< = 0 */
                    if (beta->equal_to(number_zero)) {
                        for (auto& each_integral: integrals) {
                            each_integral->zero_entries();
                        }
                    }
                    /* integrals <= beta*integrals */
                    else if (!beta->equal_to(number_one)) {
                        for (auto& each_integral: integrals) {
                            each_integral->scale(beta);
                        }
                    }
                }
                else {
    ing PartialDifferentiation = std::vector<std::pair<std::shared_ptr<Symbol>,unsigned int>>;
                    auto bra_basis = oper->get_bra()->get_basis_function();
                    auto ket_basis = oper->get_ket()->get_basis_function();
                    auto derivatives = oper->get_derivatives();
                    tSymbol::Integral::create();
                    /* integrals <= alpha*diffp(<bra|oper|ket>) */
                    if (beta->equal_to(number_zero)) {
                    }
                    /* integrals <= alpha*diffp(<bra|oper|ket>) + beta*integrals */
                    else {
                        if (!beta->equal_to(number_one)) {
                            for (auto& each_integral: integrals) {
                                each_integral->scale(beta);
                            }
                        }
                    }
                }
            }
            //density matrix: trace[]
            //mo coeff.: \sum_{I}<\phi_{I}|O|\phi_{I}>=\sum_{I}<\sum_{\mu}C_{\mu I}\chi_{\mu}|O|\sum_{\nu}C_{\nu I}\chi_{\nu}>
            //=\sum_{I}\sum_{\mu\nu}C_{\mu I}^{*}C_{\nu I}<\chi_{\mu}|O|\chi_{\nu}>
            virtual bool eval(const std::shared_ptr<tSymbolic::Number>& scaleConst,
                              const std::shared_ptr<OneElecOper>& oper,
                              const std::
                              const std::shared_ptr<BasisSet>& bra,
                              const std::shared_ptr<BasisSet>& ket,
                              const PartialDifferentiation& var,
                              std::vector<tSymbolic::Symbol>& expectations) noexcept
            {
                std::shared_ptr<Symbol>(new tSymbol::Integral());
            }
    };
}
