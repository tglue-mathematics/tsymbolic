/* tIntegral: not only an integral computation library
   Copyright 2018, 2019 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of one-electron integrand.

   2019-10-02, Bin Gao:
   * first version
*/

//FIXME: move to tSymbolic

#pragma once

#include <iterator>
#include <memory>
#include <string>

#include "tSymbolic/QuantChem.hpp"

namespace tIntegral
{
    //FIXME: it may be straightforward to do symbolic derivation of recurrence relations by introducing OneElecIntegrand?
    class OneElecIntegrand: virtual public tSymbolic::QuantChem::Integrand
                            public std::enable_shared_from_this<OneElecIntegrand>
    {
        public:
            explicit OneElecIntegrand(const std::shared_ptr<OneElecOper> oneOper,
                                      const std::shared_ptr<BasisFunction> braBasis,
                                      const std::shared_ptr<BasisFunction> ketBasis) noexcept:
                m_oper(oneOper),
                m_bra(braBasis),
                m_ket(ketBasis) {}
            inline derivative;
            inline operator;
            inline basis_set;
            virtual std::string to_string() const noexcept override
            {
                return "{bra: "+m_bra->to_string()
                    +", operator: "+m_oper->to_string()
                    +", ket: "+m_ket->to_string()+'}';
            }

            /* @fn:df[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const PartialDifferentiation& var) const noexcept override
            {
                for (auto const& each_var: var) {
                    if (is_zero_derivative(each_var.first, each_var.second)) {
                        return std::make_shared<Symbol>(Number(0));
                    }
                }
                return std::make_shared<Symbol>(PartialDerivative(get_shared(), var));
            }

            virtual std::string type_name() const noexcept override
            {
                return std::string("OneElecIntegrand");
            }
            /* @fn:get_indices[Return all indices that the integrand depends on.]
               @return:[Indices of the integrand.] */
            virtual std::vector<std::shared_ptr<IntegrandIndex>> get_all_indices() const noexcept override
            {
                auto all_indices = m_bra->get_all_indices();
                auto indices = m_ket->get_all_indices();
                all_indices.reserve(all_indices.size()+indices.size());
                all_indices.insert(all_indices.end(),
                                   std::make_move_iterator(indices.begin()),
                                   std::make_move_iterator(indices.end()));
                indices = m_oper->get_all_indices();
                all_indices.reserve(all_indices.size()+indices.size());
                all_indices.insert(all_indices.end(),
                                   std::make_move_iterator(indices.begin()),
                                   std::make_move_iterator(indices.end()));
                return all_indices;
            }
            /* @fn:get_all_orders[Get orders of all indices.]
               @return:[Orders of all indices.] */
            virtual std::vector<unsigned int> get_all_orders() const noexcept override
            {
                auto all_orders = m_bra->get_all_orders();
                auto orders = m_ket->get_all_orders();
                all_orders.reserve(all_orders.size()+orders.size());
                all_orders.insert(all_orders.end(),
                                  std::make_move_iterator(orders.begin()),
                                  std::make_move_iterator(orders.end()));
                orders = m_oper->get_all_orders();
                all_orders.insert(all_orders.end(),
                                  std::make_move_iterator(orders.begin()),
                                  std::make_move_iterator(orders.end()));
                return all_orders;
            }
            /* @fn:get_num_centers[Get the number of centers of the integrand.]
               @return:[Number of centers.] */
            virtual unsigned int get_num_centers() const noexcept override
            {
                return m_bra->get_num_centers()+m_ket->get_num_centers()+m_oper->get_num_centers();
            }
        protected:
            virtual std::shared_ptr<Symbol> get_shared() const noexcept override
            {
                return shared_from_this();
            }
            virtual bool is_zero_derivative(const std::shared_ptr<Symbol>& var,
                                            const unsigned int& order) const noexcept = 0;
        private:
            /* Expansion coefficient, expanded partial differentiation on bra, ket and operator */
            std::vector<std::pair<unsigned int,std::array<PartialDifferentiation,3>>> m_expanded_diff;

            std::shared_ptr<BasisFunction> m_bra;
            std::shared_ptr<BasisFunction> m_ket;
            std::shared_ptr<OneElecOper> m_oper;
    };
}
