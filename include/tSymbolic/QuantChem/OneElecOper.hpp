/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of one-electron operator class.

   2019-09-27, Bin Gao:
   * moved into the namespace tSymbolic::Chemistry

   2018-05-05, Bin Gao:
   * first version
*/

#pragma once

#include <array>
#include <memory>
#include <string>
#include <typeindex>
#include <utility>
#include <vector>

#include "tGlueCore/Stringify.hpp"

#include "tSymbolic/QuantChem/Integrand.hpp"
#include "tSymbolic/QuantChem/IntegrandIndex.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tSymbolic
{
    namespace QuantChem
    {
        /* == Cartesian Multipole Moments

           The first *concrete* one-electron operator is Cartesian multipole
           moments, which can also be used to compute overlap integrals.

           IMPORTANT: The validity of one-electron operators for periodic systems
           and the use of London phase factors need to be considered, either at
           the level of operators or integrators. It is still under consideration.

           @class:CartMultMoment[Cartesian multipole moments.]

           Cartesian multipole moments in the library tIntegral are defined as
           stem:[
             \bar{C}\boldsymbol{r}_{M}^{\boldsymbol{m}}
             \boldsymbol{\partial}_{\boldsymbol{r}}^{\boldsymbol{n}}
           ], where

           [horizontal]
           stem:[\bar{C}]:: Scale constant.
           stem:[\boldsymbol{M}]:: Origin of multipole expansion. The dipole's
             origin is usually taken at the center of mass to simplify the treatment
             of rotational-electronic coupling, see
             "`Theory of Molecular Rydberg States`" by M. S. Child, pp. 78 (2011).
           stem:[\boldsymbol{m}]:: Order of Cartesian multipole moments.
           stem:[\boldsymbol{n}]:: Order of derivatives with respect to electron coordinates.

           Cartesian multipole moments can be created by its constructor as follows, */
        class CartMultMoment: virtual public Integrand,
                              public std::enable_shared_from_this<CartMultMoment>
        {
            public:
                /* @fn:CartMultMoment()[Constructor.]
                   @param[in]:scaleConst[Scale constant stem:[\bar{C}].]
                   @param[in]:idxOrigin[Index of the origin of multipole expansion stem:[\boldsymbol{M}].]
                   @param[in]:coordOrigin[Coordinates of the origin stem:[\boldsymbol{M}].]
                   @param[in]:orderMultipole[Order of Cartesian multipole moments (stem:[\boldsymbol{m}]).]
                   @param[in]:orderElectronic[Order of derivatives with respect to electron coordinates (stem:[\boldsymbol{n}]).] */
                explicit CartMultMoment(const double scaleConst=1.0,
                                        const int idxOrigin=-1,
                                        const std::array<double,3> coordOrigin=std::array<double,3>{0,0,0},
                                        const unsigned int orderMultipole=0,
                                        const unsigned int orderElectronic=0) noexcept:
                    m_scale_const(scaleConst),
                    m_idx_origin(idxOrigin),
                    m_coord_origin(coordOrigin),
                    m_order_multipole(orderMultipole),
                    m_order_electronic(orderElectronic) {}
                /* @fn:~CartMultMoment()[Deconstructor.] */
                virtual ~CartMultMoment() noexcept = default;
                virtual std::string to_string() const noexcept override
                {
                    return "{scale-const: "+std::to_string(m_scale_const)
                        +", origin-index: "+std::to_string(m_idx_origin)
                        +", origin-coordinates: "+tGlueCore::stringify(m_coord_origin.cbegin(), m_coord_origin.cend())
                        +", multipole-order: "+std::to_string(m_order_multipole)
                        +", electronic-order: "+std::to_string(m_order_electronic)+'}';
                }
                /* The following functions are required by the one-electron
                   operator base class, and can be futher overridden by any derived
                   class of `CartMultMoment`.

                   @fn:type_id[Get the runtime type of Cartesian multipole moments.]
                   @return:[Runtime type of Cartesian multipole moments.] */
                virtual std::type_index type_id() const noexcept override
                {
                    return typeid(*this);
                }
                virtual std::string type_name() const noexcept override
                {
                    return std::string("CartMultMoment");
                }
                /* @fn:get_indices[Return all indices that the integrand depends on.]
                   @return:[Indices of the integrand.] */
                virtual std::vector<std::shared_ptr<IntegrandIndex>> get_all_indices() const noexcept = 0;
                {
                    return std::vector<std::shared_ptr<IntegrandIndex>>({
                        std::make_shared<IntegrandIndex>(IntegrandTriangularIndex(std::string("MultipoleMoment"))),
                        std::make_shared<IntegrandIndex>(IntegrandTriangularIndex(std::string("ElectronDerivative")))
                    });
                }
                /* @fn:get_all_orders[Get orders of all indices.]
                   @return:[Orders of all indices.] */
                virtual std::vector<unsigned int> get_all_orders() const noexcept override
                {
                    return std::vector<unsigned int>({m_order_multipole, m_order_electronic});
                }
                /* Last but not least, there are a few functions specialized for
                   the Cartesian multipole moments.

                   @fn:get_scale_const[Get scale constant.]
                   @return:[The scale constant.] */
                virtual double get_scale_const() const noexcept { return m_scale_const; }
                /* @fn:get_idx_origin[Get index of the origin of multipole expansion.]
                   @return:[Index of the origin of multipole expansion.] */
                virtual int get_idx_origin() const noexcept { return m_idx_origin; }
                /* @fn:get_coord_origin[Get coordinates of the origin of multipole expansion.]
                   @return:[Coordinates of the origin of multipole expansion.] */
                virtual std::array<double,3> get_coord_origin() const noexcept { return m_coord_origin; }
            protected:
                virtual std::shared_ptr<tSymbolic::Symbol> get_shared() const noexcept override
                {
                    return shared_from_this();
                }
                /* @fn:is_zero_derivative[Check if zero derivative resulted.]
                   @param[in]:var[The given (physical) variable.]
                   @return:[Boolean indicates if the derivative is zero.] */
                virtual bool is_zero_derivative(const std::shared_ptr<Symbol>& var,
                                                const unsigned int& order) const noexcept override
                {
                    switch(var->type_id()) {
                        case typeid(NuclearPosition):
                            return m_idx_origin<0 || m_order_multipole<order;
                        default:
                            return true;
                    }
                }
            private:
                /*@@ Internal data for representing Cartesian multipole moments. */
                double m_scale_const;
                int m_idx_origin;
                std::array<double,3> m_coord_origin;
                unsigned int m_order_multipole;
                unsigned int m_order_electronic;
        };

        /* @class:NucAttractPotential[Nuclear attraction potential.] */
        class NucAttractPotential: virtual public Integrand,
                                   public std::enable_shared_from_this<NucAttractPotential>
        {
            public:
                /* @fn:NucAttractPotential()[Constructor.] */
                explicit NucAttractPotential(const double scaleConst=1.0,
                                             const int idxNucleus=0,
                                             const std::array<double,3> coordNucleus=std::array<double,3>{0,0,0},
                                             const int idxOrigin=-1,
                                             const std::array<double,3> coordOrigin=std::array<double,3>{0,0,0},
                                             const unsigned int orderMultipole=0,
                                             const unsigned int orderElectronic=0) noexcept:
                    m_scale_const(scaleConst),
                    m_idx_nucleus(idxNucleus),
                    m_coord_nucleus(coordNucleus),
                    m_idx_origin(idxOrigin),
                    m_coord_origin(coordOrigin),
                    m_order_multipole(orderMultipole),
                    m_order_electronic(orderElectronic) {}
                /* @fn:~NucAttractPotential()[Deconstructor.] */
                virtual ~NucAttractPotential() noexcept = default;
                virtual std::string to_string() const noexcept override
                {
                    return "{scale-const: "+std::to_string(m_scale_const)
                        +", nuclear-index: "+std::to_string(m_idx_nucleus)
                        +", nuclear-coordinates: "+tGlueCore::stringify(m_coord_nucleus.cbegin(), m_coord_nucleus.cend())
                        +", origin-index: "+std::to_string(m_idx_origin)
                        +", origin-coordinates: "+tGlueCore::stringify(m_coord_origin.cbegin(), m_coord_origin.cend())
                        +", multipole-order: "+std::to_string(m_order_multipole)
                        +", electronic-order: "+std::to_string(m_order_electronic)+'}';
                }
                /* @fn:type_id[Get the runtime type of nuclear attraction potential.]
                   @return:[Runtime type of nuclear attraction potential.] */
                virtual std::type_index type_id() const noexcept override
                {
                    return typeid(*this);
                }
                virtual std::string type_name() const noexcept override
                {
                    return std::string("NucAttractPotential");
                }
                /* @fn:get_indices[Return all indices that the integrand depends on.]
                   @return:[Indices of the integrand.] */
                virtual std::vector<std::shared_ptr<IntegrandIndex>> get_all_indices() const noexcept = 0;
                {
                    return std::vector<std::shared_ptr<IntegrandIndex>>({
                        std::make_shared<IntegrandIndex>(IntegrandScalarIndex(std::string("BoysFunction"))),
                        std::make_shared<IntegrandIndex>(IntegrandTriangularIndex(std::string("MultipoleMoment"))),
                        std::make_shared<IntegrandIndex>(IntegrandTriangularIndex(std::string("ElectronDerivative")))
                    });
                }
                /* @fn:get_all_orders[Get orders of all indices.]
                   @return:[Orders of all indices.] */
                virtual std::vector<unsigned int> get_all_orders() const noexcept override
                {
                    return std::vector<unsigned int>({0, m_order_multipole, m_order_electronic});
                }
            protected:
                virtual std::shared_ptr<tSymbolic::Symbol> get_shared() const noexcept override
                {
                    return shared_from_this();
                }
                /* @fn:is_zero_derivative[Check if zero derivative resulted.]
                   @param[in]:var[The given (physical) variable.]
                   @return:[Boolean indicates if the derivative is zero.] */
                virtual bool is_zero_derivative(const std::shared_ptr<Symbol>& var,
                                                const unsigned int& order) const noexcept override
                {
                    switch (var->type_id()) {
                        case typeid(NuclearPosition):
                            return m_idx_nucleus<0 && (m_idx_origin<0 || m_order_multipole<order);
                        default:
                            return true;
                    }
                }
            private:
                double m_scale_const;
                int m_idx_nucleus;
                std::array<double,3> m_coord_nucleus;
                int m_idx_origin;
                std::array<double,3> m_coord_origin;
                unsigned int m_order_multipole;
                unsigned int m_order_electronic;
        };
    }
}
