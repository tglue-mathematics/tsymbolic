/* tIntegral: not only an integral computation library
   Copyright 2018, 2019 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of one-electron operator class.

   2019-04-09, Bin Gao:
   * change to visitor pattern, because usually users do not need to implement
     routines for new one-electron operators by themselves; as such, the class
     OneElecEngine is merged into OneElecRepresentation

   2019-02-08, Bin Gao:
   * new design by introducing classes OneElecEngine and OneElecRepresentation

   2019-02-06, Bin Gao:
   * built on top of Delegate class and Symbol class

   2018-05-05, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <typeindex>
#include <type_traits>
#include <utility>
#include <vector>

#include "tGlueCore/Delegate.hpp"

#include "tSymbolic/QuantChem/Integrand.hpp"
#include "tSymbolic/QuantChem/IntegrandIndex.hpp"

#include "tMatrix/BlockMat.hpp"

//FIXME: move to OpenRSP

/*@

  @!:[:stem: latexmath] */
namespace tSymbolic
{
    namespace QuantChem
    {
        /* == Separation of One-Electron Operator and Integrator Classes

           One-electron operators are one of the key components in the library
           tIntegral. The top-level design is to have one base class for all
           *concrete* one-electron operator derived classes. This one-electron
           operator base class can be used in applications using tIntegral.

           As a one-electron operator class, there are two fundamental
           requirements:

           . can be used in symbolic computation, for instance can be
             differentiated with respect to given perturbations, and
           . can be evaluated numerically with given molecular system and basis
             functions.

           The former requirement can be accomplished by making the one-electron
           operator base and derived classes inherited from the `Symbol` class of
           the library
           link:https://gitlab.com/tglue-mathematics/tsymbolic[tSymbolic]. As such,
           all one-electron operators can be treated as symbols, and can be used,
           for instance in response theory.

           The latter requirement needs numerical integration engine(s) that will
           be implemented in tIntegral library. Let us name such engine(s) as
           integrator class(es).

           The first principle of detailed design is to separate one-electron
           operator classes from their integrator classes. One-electron operator
           classes should only concern with necessary information related to the
           operators, and integrator classes take care of given molecular system
           and basis functions, and evaluate integrals and expectation values with
           information from one-electron operator classes.

           The above design principle make it possible to numerically evaluate a
           one-electron operator with different molecular systems and basis
           functions by feeding them all into a one-electron integrator.

           The second concern is about feasibility and flexibility. Let us consider
           using the library tIntegral in a self-consistent field (SCF) solver. One
           can

           * either implement a one-electron integrator base class and send one of
             its instance to the SCF solver, and the solver will invoke it with any
             operator,
           * or implement different one-electron integrator classes for different
             one-electron operators and encapsulated integrator instance(s) into
             each operator, and the solver will invoke some member function(s) of
             each operator.

           I (Bin Gao) prefer the latter, as it is more flexibale and its
           implementation should be more straightforward than the former.

           == Integral and Expectation Value Computation Engines

           The separation between one-electron operator classes and their
           integrator classes further means that

           * one-electron operator classes will not depend on integrator classes,
             and
           * one-electron operator classes will also not depend on molecular system
             and basis function classes that are needed in integrator classes.

           Based on this design philosophy, I will build one-electron operator
           classes on top of `Delegate` class in the library
           link:https://gitlab.com/tglue/tglue-core[tGlueCore]. Currently, each
           operator class contains two `Delegate` objects that work as backend
           computation engines for

           * integral computation, and
           * expectation value computation.

           These `Delegate` objects will be created by users and given to
           one-electron operators. To reduce the effort of creating these
           `Delegate` objects, there can be *a template factory function*
           developed to create default integrator instance(s) for a given
           molecular system and its basis functions, and given one-electron
           operator(s).

           The design of one-electron integrator classes and the above template
           factory function will be addressed in the secion of class
           `OneElecIntegrator`. I will describe these `Delegate` objects in the
           current section.

           Because the molecular system and basis functions will be taken care by
           one-electron integrator classes, the aforementioned encapsulated
           `Delegate` objects only need to take a few arguments as:

           * Integral computation engine
             ** operator (containing derivatives),
             ** computed integrals;
           * Expectation value computation engine
             ** operator (containing derivatives),
             ** molecular states,
             ** computed expectation values.

           Types of these two engines can be easily implemented as the following
           template types `OneIntEngine` and `OneExpEngine`:

           @typedef:OneIntEngine[Type of integral computation engines for a one-electron operator.]
           @tparam:OneOperType[Type of the one-electron operator.] */
        using OneIntEngine = tGlueCore::Delegate<IntegratorState(const std::shared_ptr<OneElecRepresentation>&,
                                                                 std::vector<tMatrix::BlockMat>&)>;
        /* @typedef:OneExpEngine[Type of expectation value computation engines for a one-electron operator.]
           @tparam:OneOperType[Type of the one-electron operator.]

           The return types of these two engines are the enum class
           `IntegratorState`, which indicates whether the engines work well or with
           any error. */
        using OneExpEngine = tGlueCore::Delegate<IntegratorState(const std::shared_ptr<OneElecRepresentation>&,
                                                                 const std::vector<tMatrix::BlockMat>&,
                                                                 std::vector<tReal>&)>;
        /* == One-Electron Operator Base Class

           The one-electron operator base class `OneElecRepresentation` is inherited from the
           `Symbol` class of the library
           link:https://gitlab.com/tglue-mathematics/tsymbolic[tSymbolic], and can
           be used in applications using tIntegral. For instance, an SCF solver can
           accept a vector of base class `OneElecRepresentation` smart pointers, and member
           functions of one-electron operator derived classes can be invoked during
           run time (during the SCF calculation).

           However, one should note that the the base class `OneElecRepresentation` is only
           for advanced users and developers.  Other users should use one of
           concrete one-electron operator derived classes in practice.

           @class:OneElecRepresentation[Base class for all one-electron operators.] */
        template<typename TypeRepresentation>
        class OneElecRepresentation: virtual public Integrand
                                     public std::enable_shared_from_this<OneElecRepresentation>
        {
            private:
                 tGlueCore::Delegate<bool(const std::shared_ptr<OneElecOper>,
                                          const std::vector<std::shared_ptr<BasisFunction>>&,
                                          const std::vector<std::shared_ptr<BasisFunction>>&,
                                          const PartialDifferentiation&,
                                          std::vector<TypeRepresentation>&)> m_int_engine;
        };

        template<typename TypeBra,
                 typename TypeKet,
                 typename = typename std::enable_if
                 <std::is_base_of<BasisFunction, TypeBra>::value &&
                  std::is_base_of<BasisFunction, TypeKet>::value>::type>
        class OneElecRepresentation: virtual public Integrand
                                     public std::enable_shared_from_this<OneElecRepresentation>
        {
            public:
                /* @fn:OneElecRepresentation()[Constructor.] */
                explicit OneElecRepresentation(const std::shared_ptr<OneElecOper> oneOper,
                                               const std::vector<std::shared_ptr<TypeBra>> braBasis,
                                               const std::vector<std::shared_ptr<TypeBra>> ketBasis) noexcept:
                    m_bra(braBasis),
                    m_ket(ketBasis),
                    m_integrand(OneElecIntegrand(oneOper, braBasis.front(), ketBasis.front())) {}
                /* @fn:~OneElecRepresentation()[Deconstructor.] */
                virtual ~OneElecRepresentation() noexcept = default;
                /* @fn:df[Take partial differentiation.] */
                virtual std::shared_ptr<Symbol> diffp(const PartialDifferentiation& var) const noexcept override
                {
                    return m_integrand.diffp(var);
                }
                /* @fn:set_engine[Set integral and expectation value computation engines.]
                   @param[in]:intEngine[An integral computation engine.]
                   @param[in]:expEngine[An expectation value computation engine.]

                   This function assigns given computation engines to protected
                   member computation engines that can be used by one-electron
                   operator derived classes. */
                inline void set_engine(const OneIntEngine intEngine, const OneExpEngine expEngine) noexcept
                {
                    m_int_engine = intEngine;
                    m_exp_engine = expEngine;
                }
                /* @fn:has_int_engine[Check if the integral computation engine is set.]
                   @return:[`true` if the integral computation engine is set, false otherwise.] */
                inline bool has_int_engine() const noexcept
                {
                    return m_int_engine ? true : false;
                }
                /* @fn:has_exp_engine[Check if the expectation value computation engine is set.]
                   @return:[`true` if the expectation value computation engine is set, false otherwise.] */
                inline bool has_exp_engine() const noexcept
                {
                    return m_exp_engine ? true : false;
                }
                /* @fn:get_integral[Evaluate integrals.]
                   @param[inout]:intOper[Integral matrices.]
                   @return:[State of the integrator that performs the evaluation.]

                   This function `get_integral()` uses template method pattern that
                   one-electron operator derived classes need to implement:

                   * a protected member function `invoke_int_engine()` to
                     invoke the computation engine. */
                inline IntegratorState get_integral(std::vector<tMatrix::BlockMat>& intOper) noexcept
                {
                    if (m_int_engine) {
                        IntegratorState state_integrator;
                        switch(invoke_int_engine(state_integrator, intOper)) {
                            case tGlueCore::DelegateState::Callable:
                                return state_integrator;
                            case tGlueCore::DelegateState::Invoked:
                                return IntegratorState::InvokedEngine;
                            default:
                                return IntegratorState::UnknownError;
                        }
                    }
                    else {
                        return IntegratorState::EmptyEngine;
                    }
                }
                /* @fn:get_expectation[Evaluate expectation values with given states.]
                   @param[in]:states[Given states.]
                   @param[inout]:expOper[Expectation values.]
                   @return:[State of the integrator that performs the evaluation.]

                   The template method pattern is also used here for
                   the expectation value computation:

                   * a protected member function `invoke_exp_engine()` to
                     invoke the computation engine. */
                inline IntegratorState get_expectation(const std::vector<tMatrix::BlockMat>& states,
                                                       std::vector<tReal>& expOper) noexcept
                {
                    if (m_exp_engine) {
                        IntegratorState state_integrator;
                        switch(invoke_exp_engine(state_integrator, states, expOper)) {
                            case tGlueCore::DelegateState::Callable:
                                return state_integrator;
                            case tGlueCore::DelegateState::Invoked:
                                return IntegratorState::InvokedEngine;
                            default:
                                return IntegratorState::UnknownError;
                        }
                    }
                    else {
                        return IntegratorState::EmptyEngine;
                    }
                }
                /* Following virtual functions will be implemented in different
                   one-electron operator derived classes, and they are necessary
                   for various situations, such as integral and expectation value
                   computations.

                   @fn:has_dependence[Check if the operator depends on a given (physical) variable.]
                   @param[in]:variable[The given (physical) variable.]
                   @return:[Boolean indicates the dependence.] */
                virtual bool has_dependence(const std::shared_ptr<tSymbolic::Symbol> variable) const noexcept = 0;

/*TO    DO: London phase factor, periodic system*/
//vi    rtual inline bool has_london_phasor(const std::shared_ptr<tSymbolic::Symbol>) const noexcept = 0;
            protected:
                /*@@ Integral and expectation value computation engines. */
                OneIntEngine m_int_engine;
                OneExpEngine m_exp_engine;

                /* Last, one-electron operator derived classes need to implement
                   following protected member functions to invoke integral and
                   expectation value computation engines according to the template
                   method design pattern.

                   @fn:invoke_int_engine[Invoke the integral computation engine.]
                   @param[inout]:stateIntegrator[State of the integrator that performs the computation.]
                   @param[inout]:intOper[Integral matrices.]
                   @return:[State of the delegate that wraps the integrator.] */
                virtual tGlueCore::DelegateState
                invoke_int_engine(IntegratorState& stateIntegrator,
                                  std::vector<tMatrix::BlockMat>& intOper) noexcept = 0;
                /* @fn:invoke_exp_engine[Invoke the expectation value computation engine.]
                   @param[inout]:stateIntegrator[State of the integrator that performs the computation.]
                   @param[in]:states[Given states.]
                   @param[inout]:expOper[Expectation values.]
                   @return:[State of the delegate that wraps the integrator.] */
                virtual tGlueCore::DelegateState
                invoke_exp_engine(IntegratorState& stateIntegrator,
                                  const std::vector<tMatrix::BlockMat>& states,
                                  std::vector<tReal>& expOper) noexcept = 0;
            private:
                OneElecIntegrand m_integrand;
                std::shared_ptr<BasisSet> m_bra;
                std::shared_ptr<BasisSet> m_ket;
                //or?
                std::vector<std::shared_ptr<TypeBra>> m_bra;
                std::vector<std::shared_ptr<TypeKet>> m_ket;
        };
    }
}
