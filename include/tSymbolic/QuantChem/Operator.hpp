/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of quantum operator class.

   2020-05-03, Bin Gao:
   * first version
*/

#pragma once

#include <memory>

#include "tSymbolic/Symbol.hpp"

/*@

  @!:[:stem: latexmath] */
namespace tSymbolic
{
    ///* @class:ZeroElecOperator[Base class for all zero-electron operators.] */
    //class ZeroElecOperator: virtual public Symbol
    //{
    //    public:
    //        explicit ZeroElecOperator() noexcept = default;
    //        virtual ~ZeroElecOperator() noexcept = default;
    //};

    /* == Electron operator classes

       @class:OneElecOperator[Base class for all one-electron operators.] */
    class OneElecOperator: virtual public Symbol
    //or class OneElecOperator: virtual public Matrix
    {
        public:
            /* @fn:OneElecOperator()[Constructor.] */
            explicit OneElecOperator() noexcept = default;
            /* @fn:~OneElecOperator()[Deconstructor.] */
            virtual ~OneElecOperator() noexcept = default;
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override
            {
                if (m_bra) {
                    if (m_ket) {
                        return m_bra->max_diff_order(var)
                            +oper_max_diff_order(var)
                            +m_ket->max_diff_order(var);
                    }
                    else {
                        return m_bra->max_diff_order(var)+oper_max_diff_order(var);
                    }
                }
                else {
                    if (m_ket) {
                        return oper_max_diff_order(var)+m_ket->max_diff_order(var);
                    }
                    else {
                        return oper_max_diff_order(var);
                    }
                }
            }
            /* @fn:set_representation[Set basis set representation.] */
            inline void set_representation(const std::shared_ptr<Symbol> bra,
                                           const std::shared_ptr<Symbol> ket) noexcept
            {
                m_bra = bra;
                m_ket = ket;
            }
            /* @fn:get_bra[Get basis set on the bra center.] */
            inline std::shared_ptr<Symbol> get_bra() const noexcept { return m_bra; }
            /* @fn:get_bra[Get basis set on the ket center.] */
            inline std::shared_ptr<Symbol> get_ket() const noexcept { return m_ket; }
        protected:
            /* @fn:oper_max_diff_order[Get the maximum order of differentiation of the operator with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation of the operator.] */
            virtual unsigned int oper_max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept = 0;
        private:
            std::shared_ptr<Symbol> m_bra;
            std::shared_ptr<Symbol> m_ket;
    };

    /*FIXME: one-electron density matrix for OpenRSP*/

    ///* @class:TwoElecOperator[Base class for all two-electron operators.] */
    //class TwoElecOperator: virtual public Symbol
    //{
    //    public:
    //        explicit TwoElecOperator(const std::shared_ptr<Symbol> densityMatrix) noexcept:
    //            m_density_matrix(densityMatrix) {}
    //        virtual ~TwoElecOperator() noexcept = default;
    //        /* @fn:diffp[Take partial differentiation.] */
    //        virtual std::shared_ptr<Symbol> diffp(const PartialDifferentiation& var) const noexcept override
    //        {
    //        }
    //    private:
    //        std::shared_ptr<Symbol> m_density_matrix;
    //};

    ///* @class:ExchCorrFunctional[Base class for all exchange-correlation functionals.] */
    //class ExchCorrFunctional: virtual public Symbol
    //{
    //    public:
    //        explicit ExchCorrFunctional(const std::shared_ptr<Symbol> elecDensity) noexcept:
    //            m_elec_density(elecDensity) {}
    //        virtual ~ExchCorrFunctional() noexcept = default;
    //    private:
    //        std::shared_ptr<Symbol> m_elec_density;
    //};
}
