/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of SymbolSubstitution and FunctionSubstitution
   classes.

   2020-04-22, Bin Gao:
   * first version
*/

#pragma once

#include <iterator>
#include <memory>
#include <string>
#include <type_traits>
#include <utility>
#include <vector>

#include "tSymbolic/Symbol.hpp"

namespace tSymbolic
{
    inline std::string stringify(const SymbolSubstitution& substitutions) noexcept
    {
        if (substitutions.size()==0) return std::string("[]");
        std::string str_subs;
        for (auto const& each_sub: substitutions) {
            str_subs += "{variable: "+each_sub.first->to_string()
                      + ", substitution: "+each_sub.second->to_string()+"}, ";
        }
        return "["+str_subs.substr(0, str_subs.size()-2)+"]";
    }

    inline std::string stringify(const FunctionSubstitution& substitutions) noexcept
    {
        if (substitutions.size()==0) return std::string("[]");
        std::string str_subs;
        for (auto const& each_sub: substitutions) {
            str_subs += "{variable: "+each_sub.first->to_string()
                      + ", substitution: "+each_sub.second.target_type().name()+"}, ";
        }
        return "["+str_subs.substr(0, str_subs.size()-2)+"]";
    }

    inline std::shared_ptr<Symbol> subs(const std::shared_ptr<Symbol>& expr,
                                        const SymbolSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (expr->equal_to(each_sub.first)) return each_sub.second;
        }
        /* We try to substitute symbols in the expression if itself was not substituted */
        return expr->subs(substitutions);
    }

    inline std::shared_ptr<Symbol> subs(const std::shared_ptr<Symbol>& expr,
                                        const FunctionSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (expr->equal_to(each_sub.first)) return each_sub.second(expr);
        }
        /* We try to substitute symbols in the expression if itself was not substituted */
        return expr->subs(substitutions);
    }

    template<typename T, typename SubstitutionType>
    inline typename std::enable_if<std::is_same<typename std::iterator_traits<T>::value_type,
                                                std::shared_ptr<Symbol>>::value &&
                                   (std::is_same<SubstitutionType, SymbolSubstitution>::value ||
                                    std::is_same<SubstitutionType, FunctionSubstitution>::value),
                                   std::vector<std::shared_ptr<Symbol>>>::type
    subs(T beginExpr, T endExpr, const SubstitutionType& substitutions) noexcept
    {
        std::vector<std::shared_ptr<Symbol>> expr_subs;
        expr_subs.reserve(std::distance(beginExpr, endExpr));
        for (; beginExpr!=endExpr; ++beginExpr) {
            expr_subs.push_back(subs(*beginExpr, substitutions));
        }
        return expr_subs;
    }
}
