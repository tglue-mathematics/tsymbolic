/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of symbol base class.

   2019-02-06, Bin Gao:
   * first version
*/

#pragma once

#include <functional>
#include <memory>
#include <string>
#include <typeindex>
#include <utility>
#include <vector>

/*@

  @!:[:stem: latexmath]

  @namespace:tSymbolic[Namespace of tSymbolic library.] */
namespace tSymbolic
{
    //https://en.wikipedia.org/wiki/Integration_by_parts
    //https://en.wikipedia.org/wiki/Gaussian_integral

    /* Forward declaration */
    class Symbol;
    class Number;

    using VariableOrder = std::vector<std::pair<std::shared_ptr<Symbol>,unsigned int>>;
    /* No duplicated symbol substituted is allowed */
    using SymbolSubstitution = std::vector<std::pair<std::shared_ptr<Symbol>,std::shared_ptr<Symbol>>>;
    using FunctionSubstitution
        = std::vector<std::pair<std::shared_ptr<Symbol>,std::function<std::shared_ptr<Symbol>(const std::shared_ptr<Symbol>&)>>>;

    /* @class:Symbol[Base class of all symbolic variables, functions, equations and expressions.] */
    class Symbol: public std::enable_shared_from_this<Symbol>
    {
        public:
            /* @fn:Symbol()[Constructor.] */
            explicit Symbol() noexcept = default;
            /* @fn:~Symbol()[Deconstructor.] */
            virtual ~Symbol() noexcept = default;
            /* @fn:get_shared[Get shared pointer of the instance.] */
            virtual std::shared_ptr<Symbol> get_shared() noexcept;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept;
            /*FIXME: may implement using some external class?*/
            virtual std::string to_string() const noexcept;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept;
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept;
            /* @fn:diffp[Take partial differentiation.]
               var contains unique variable and non-zero order */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept;
            /* @fn:subs[Return a new symbol by substituting specified symbols with other given ones.] */
            virtual std::shared_ptr<Symbol> subs(const SymbolSubstitution& substitutions) noexcept;
            /* @fn:subs[Return a new symbol by substituting specified symbols with given std::function's, and each std::function will take the corresponding specified symbol as input.]*/
            virtual std::shared_ptr<Symbol> subs(const FunctionSubstitution& substitutions) noexcept;
            /* @fn:get_layout[Get the layout of components of the symbol with a given order.] */
            virtual std::vector<VariableOrder> get_layout(const unsigned int order) noexcept;
            ///* @fn:set_dependence[Set dependence.]
            //   order can be set as std::numeric_limits<unsigned int>::max() */
            //virtual void set_dependence(std::shared_ptr<Symbol> var,
            //                                   const unsigned int order) noexcept
            //{
            //    auto dep_var = m_vars.find(var);
            //    if (dep_var==m_vars.end()) {
            //        m_vars.emplace(var, order);
            //    }
            //    else {
            //        dep_var->second = order;
            //    }
            //}
            ///* @fn:set_dependence[Set dependence.] */
            //virtual void
            //set_dependence(const std::vector<std::pair<std::shared_ptr<Symbol>,unsigned int>>& var) noexcept
            //{
            //    for (auto const& dep_var: var) {
            //        set_dependence(dep_var.first, dep_var.second);
            //    }
            //}
            /* @fn:type_id[Get the runtime type of a symbol.]
               @return:[Runtime type of the symbol.] */
            virtual std::type_index type_id() const noexcept;
        //    virtual bool
        //    has_dependence(const std::vector<std::pair<std::shared_ptr<Symbol>,unsigned int>>& var) const noexcept
        //    {
        //        for (auto const& each_var: var) {
        //            auto dep_var = m_vars.find(each_var.first);
        //            if (dep_var==m_vars.end()) {
        //                return false;
        //            }
        //            else if (dep_var->second<each_var.second) {
        //                return false;
        //            }
        //        }
        //        return true;
        //    }
        //    /* @fn:get_derivatives[Return derivatives on the symbol.]
        //       @return:[Derivatives.] */
        //    inline std::vector<std::pair<std::shared_ptr<Symbol>,unsigned int>> get_derivatives() const noexcept
        //    {
        //        return m_derivatives;
        //    }
        //private:
        //    std::vector<std::pair<std::shared_ptr<Symbol>,unsigned int>> m_derivatives;
        //private:
        //    std::unordered_map<std::shared_ptr<Symbol>,unsigned int> m_vars;
    };
}
