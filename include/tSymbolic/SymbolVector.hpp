/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of symbol vector.

   2020-05-15, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/Vector.hpp"
#include "tSymbolic/PartialDerivative.hpp"

namespace tSymbolic
{
    /* Wrap a symbol into a vector with given number of components */
    template<typename SymbolType, typename VectorType=Vector>
    class SymbolVector: virtual public Symbol
    {
        public:
            /* Nested class for the component of the vector */
            class Component: virtual public Symbol
            {
                public:
                    explicit Component(const std::shared_ptr<Symbol> symbol, const unsigned int index=0) noexcept:
                        m_symbol(symbol),
                        m_index(index) {}
                    virtual ~Component() noexcept = default;
                    /* @fn:get_copy[Get a copy of the instance.] */
                    virtual std::shared_ptr<Symbol> get_copy() noexcept override
                    {
                        return std::make_shared<Component>(std::move(*this));
                    }
                    virtual std::string to_string() const noexcept override
                    {
                        return "{symbol: "+m_symbol->to_string()+", index: "+std::to_string(m_index)+"}";
                    }
                    /* @fn:equal_to[Function for equality comparison.] */
                    virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override
                    {
                        if (other->type_id()==type_id()) {
                            /*FIXME: If pattern match works, we will remove dynamic cast */
                            std::shared_ptr<Component> other_cast = std::dynamic_pointer_cast<Component>(other);
                            return m_symbol->equal_to(other_cast->m_symbol) && m_index==other_cast->m_index;
                        }
                        else {
                            return false;
                        }
                    }
                    /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
                       @param[in]:var[The given variable.]
                       @return:[The maximum order of differentiation.] */
                    virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override
                    {
                        return m_symbol->max_diff_order(var);
                    }
                    /* @fn:diffp[Take partial differentiation.]
                       var contains unique variable and non-zero order */
                    virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override
                    {
                        return std::make_shared<PartialDerivative>(PartialDerivative(get_copy(), var));
                    }
                private:
                    std::shared_ptr<Symbol> m_symbol;
                    unsigned int m_index;
            };
            /* @fn:SymbolVector()[Constructor.]
               @param[in]:symbol[Symbol instance.] */
            explicit SymbolVector(SymbolType symbol,
                                  const unsigned int numComponents=3,
                                  const bool rowVector=false) noexcept
            {
                m_symbol = std::make_shared<SymbolType>(std::move(symbol));
                std::vector<std::shared_ptr<Symbol>> components;
                components.reserve(numComponents);
                /* Components begin from index 0 */
                for (unsigned int icomp=0; icomp<numComponents; ++icomp) {
                    components.push_back(std::make_shared<Component>(Component(m_symbol, icomp)));
                }
                m_vector = std::make_shared<VectorType>(components, rowVector);
            }
            /* @fn~SymbolVector()[Deconstructor.] */
            virtual ~SymbolVector() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override
            {
                return std::make_shared<SymbolVector<SymbolType,VectorType>>(std::move(*this));
            }
            virtual std::string to_string() const noexcept override
            {
                return m_vector->to_string();
            }
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override
            {
                if (other->type_id()==type_id()) {
                    /*FIXME: If pattern match works, we will remove dynamic cast */
                    std::shared_ptr<SymbolVector<SymbolType,VectorType>> other_cast
                        = std::dynamic_pointer_cast<SymbolVector<SymbolType,VectorType>>(other);
                    if (m_symbol->equal_to(other_cast->m_symbol)) {
                        /* No need to compare components because they both begin from index 0 */
                        return m_vector->is_same_structure(other_cast->m_vector);
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override
            {
                return m_symbol->max_diff_order(var);
            }
            /* @fn:diffp[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override
            {
                /* We only take partial differentiation on the symbol itself, not individual components */
                auto vec_diffp = std::make_shared<SymbolVector<SymbolType,VectorType>>(std::move(*this));
                vec_diffp->m_symbol = m_symbol->diffp(var);
                return vec_diffp;
            }
            /* @fn:subs[Return a new symbol by substituting specified symbols with other given ones.] */
            virtual std::shared_ptr<Symbol> subs(const SymbolSubstitution& substitutions) noexcept override
            {
                /* We only substitute the symbol itself, not individual components */
                auto symbol_subs = m_symbol->subs(substitutions);
                if (symbol_subs==m_symbol) {
                    /* Return the vector itself if no substitution made */
                    return get_shared();
                }
                else {
                    auto vec_subs = std::make_shared<SymbolVector<SymbolType,VectorType>>(std::move(*this));
                    vec_subs->m_symbol = symbol_subs;
                    return vec_subs;
                }
            }
            /* @fn:subs[Return a new symbol by substituting specified symbols with given std::function's, and each std::function will take the corresponding specified symbol as input.]*/
            virtual std::shared_ptr<Symbol> subs(const FunctionSubstitution& substitutions) noexcept override
            {
                /* We only substitute the symbol itself, not individual components */
                auto symbol_subs = m_symbol->subs(substitutions);
                if (symbol_subs==m_symbol) {
                    /* Return the vector itself if no substitution made */
                    return get_shared();
                }
                else {
                    auto vec_subs = std::make_shared<SymbolVector<SymbolType,VectorType>>(std::move(*this));
                    vec_subs->m_symbol = symbol_subs;
                    return vec_subs;
                }
            }
            /* @fn:get_layout[Get the layout of components of the symbol with a given order] */
            virtual std::vector<VariableOrder> get_layout(const unsigned int order) noexcept override
            {
                return m_vector->get_layout(order);
            }
        private:
            std::shared_ptr<Symbol> m_symbol;
            std::shared_ptr<VectorType> m_vector;
    };
}
