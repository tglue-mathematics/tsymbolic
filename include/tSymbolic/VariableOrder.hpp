/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of VariableOrder class.

   2019-09-29, Bin Gao:
   * first version
*/

#pragma once

#include <iterator>
#include <memory>
#include <typeindex>
#include <type_traits>
#include <utility>
#include <vector>

#if defined(TSYMBOLIC_DEBUG)
#include "tGlueCore/Logger.hpp"
#include "tSymbolic/Settings.hpp"
#endif

#include "tSymbolic/Symbol.hpp"

namespace tSymbolic
{
    inline VariableOrder simplify(const VariableOrder& symbols) noexcept
    {
        /* Prepare a list of unique symbols and their non-zero orders */
        VariableOrder simplified;
        for (auto each_symbol=symbols.cbegin(); each_symbol!=symbols.cend(); ++each_symbol) {
            /* Symbol with non-zero order */
            if (each_symbol->second>0) {
                bool new_symbol = true;
                for (auto each_simpl=simplified.begin(); each_simpl!=simplified.end(); ++each_simpl) {
                    if (each_symbol->first->type_id()==each_simpl->first->type_id()) {
                        each_simpl->second += each_symbol->second;
                        new_symbol = false;
                    }
                }
                if (new_symbol) {
                    simplified.push_back(std::make_pair(each_symbol->first, each_symbol->second));
                }
            }
        }
#if defined(TSYMBOLIC_DEBUG)
        if (simplified.size()!=symbols.size()) {
            std::string str_symbols("simplify(");
            for (auto const& each_symbol: symbols) {
                str_symbols += each_symbol.first->to_string()+"^{"+std::to_string(each_symbol.second)+'}';
            }
            str_symbols += ')';
            std::string str_simplified("simplify(");
            for (auto const& each_simpl: simplified) {
                str_simplified += each_simpl.first->to_string()+"^{"+std::to_string(each_simpl.second)+'}';
            }
            str_simplified += ')';
            Settings::logger->write(tGlueCore::MessageType::Debug,
                                    "simplify() is called with symbols ",
                                    str_symbols,
                                    ", and which are simplified to ",
                                    str_simplified);
        }
#endif
        return simplified;
    }

    inline VariableOrder simplify(const VariableOrder& symbols, const VariableOrder& others) noexcept
    {
        /* Prepare a list of unique symbols and their non-zero orders from the given symbols */
        auto simplified = simplify(symbols);
        /* Combine other symbols into the list of unique symbols */
        for (auto each_other=others.cbegin(); each_other!=others.cend(); ++each_other) {
            bool new_symbol = true;
            for (auto each_simpl=simplified.begin(); each_simpl!=simplified.end(); ++each_simpl) {
                if (each_other->first->type_id()==each_simpl->first->type_id()) {
                    each_simpl->second += each_other->second;
                    new_symbol = false;
                }
            }
            if (new_symbol) {
                simplified.push_back(std::make_pair(each_other->first, each_other->second));
            }
        }
        return simplified;
    }

    inline std::shared_ptr<Symbol> diffp(std::shared_ptr<Symbol>& fun,
                                         const std::shared_ptr<Symbol> var) noexcept
    {
        return fun->diffp(VariableOrder({std::pair<std::shared_ptr<Symbol>,unsigned int>(var, 1)}));
    }

    /*NOTE: will return fun itself if order is 0*/
    inline std::shared_ptr<Symbol> diffp(std::shared_ptr<Symbol>& fun,
                                         const std::shared_ptr<Symbol> var,
                                         const unsigned int order) noexcept
    {
        if (order==0) {
            return fun;
        }
        else {
            return fun->diffp(VariableOrder({std::pair<std::shared_ptr<Symbol>,unsigned int>(var, order)}));
        }
    }

    /*NOTE: will return fun itself if order is 0*/
    inline std::shared_ptr<Symbol> diffp(std::shared_ptr<Symbol>& fun, const VariableOrder& var) noexcept
    {
        /* Prepare a list of unique variables and their non-zero orders */
        auto unique_var = simplify(var);
        if (unique_var.size()==0) {
            return fun;
        }
        else {
            return fun->diffp(unique_var);
        }
    }

    /*NOTE: will return functions themselves if order is 0*/
    template<typename T>
    inline typename std::enable_if<std::is_same<typename std::iterator_traits<T>::value_type,
                                                std::shared_ptr<Symbol>>::value,
                                   std::vector<std::shared_ptr<Symbol>>>::type
    diffp(T beginFunction, T endFunction, const VariableOrder& var) noexcept
    {
        std::vector<std::shared_ptr<Symbol>> derivatives;
        derivatives.reserve(std::distance(beginFunction, endFunction));
        /* Prepare a list of unique variables and their non-zero orders */
        auto unique_var = simplify(var);
        if (unique_var.size()==0) {
            for (; beginFunction!=endFunction; ++beginFunction) {
                derivatives.push_back(*beginFunction);
            }
        }
        else {
            for (; beginFunction!=endFunction; ++beginFunction) {
                derivatives.push_back((*beginFunction)->diffp(unique_var));
            }
        }
        return derivatives;
    }

    inline void direct_product(std::vector<VariableOrder>& symbols, const std::vector<VariableOrder>& others) noexcept
    {
        auto num_symbols = symbols.size();
        for (unsigned int ivar=1; ivar<others.size(); ++ivar) {
            unsigned int new_position = symbols.size();
            symbols.insert(symbols.end(), symbols.begin(), symbols.begin()+num_symbols);
            for (; new_position<symbols.size(); ++new_position) {
                symbols[new_position].insert(symbols[new_position].end(), others[ivar].begin(), others[ivar].end());
            }
        }
        for (unsigned int new_position=0; new_position<num_symbols; ++new_position) {
            symbols[new_position].insert(symbols[new_position].end(), others[0].begin(), others[0].end());
        }
    }

    inline std::string stringify(const VariableOrder& var) noexcept
    {
        if (var.size()==0) return std::string("[]");
        std::string str_var;
        for (auto const& each_var: var) {
            str_var += "{variable: "+each_var.first->to_string()
                     + ", order: "+std::to_string(each_var.second)+"}, ";
        }
        return "["+str_var.substr(0, str_var.size()-2)+"]";
    }
}
