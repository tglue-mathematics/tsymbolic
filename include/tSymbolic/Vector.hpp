/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file is the header file of vector class.

   2020-04-22, Bin Gao:
   * first version
*/

#pragma once

#include <memory>
#include <string>
#include <vector>

#include "tSymbolic/Symbol.hpp"

namespace tSymbolic
{
    /*@ == Vector

        Many properties can be calculated as derivatives of an energy
        function with respect to specified physical variables. */
    class Vector: virtual public Symbol
    {
        public:
            explicit Vector(const std::vector<std::shared_ptr<Symbol>> elements, const bool rowVector=false) noexcept:
                m_elements(elements),
                m_row_vector(rowVector) {}
            virtual ~Vector() noexcept = default;
            /* @fn:get_copy[Get a copy of the instance.] */
            virtual std::shared_ptr<Symbol> get_copy() noexcept override;
            virtual std::string to_string() const noexcept override;
            /* @fn:equal_to[Function for equality comparison.] */
            virtual bool equal_to(const std::shared_ptr<Symbol>& other) const noexcept override;
            /* @fn:max_diff_order[Get the maximum order of differentiation with respect to a given variable.]
               @param[in]:var[The given variable.]
               @return:[The maximum order of differentiation.] */
            virtual unsigned int max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept override;
            /* @fn:diffp[Take partial differentiation.] */
            virtual std::shared_ptr<Symbol> diffp(const VariableOrder& var) noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with other given ones.] */
            virtual std::shared_ptr<Symbol> subs(const SymbolSubstitution& substitutions) noexcept override;
            /* @fn:subs[Return a new symbol by substituting specified symbols with given std::function's, and each std::function will take the corresponding specified symbol as input.]*/
            virtual std::shared_ptr<Symbol> subs(const FunctionSubstitution& substitutions) noexcept override;
            /* @fn:get_layout[Get the layout of components of the symbol with a given order.] */
            virtual std::vector<VariableOrder> get_layout(const unsigned int order) noexcept override;
            /* @fn:get_num_elements[Get the number of elements.] */
            inline unsigned int get_num_elements() const noexcept { return m_elements.size(); }
            /* @fn:is_row_vector[Check if it is a row vector.] */
            inline bool is_row_vector() const noexcept { return m_row_vector; }
            /* @fn:is_same_structure[Check if another vector has the same structure.] */
            inline bool is_same_structure(const std::shared_ptr<Vector>& other) const noexcept
            {
                return m_row_vector==other->m_row_vector && m_elements.size()==other->m_elements.size();
            }
            /* @fn:get_elements[Get elements of the vector.] */
            inline std::vector<std::shared_ptr<Symbol>> get_elements() const noexcept { return m_elements; }
            /* @fn:[][Get an element.] */
            inline std::shared_ptr<Symbol> operator[](const unsigned int position) const noexcept
            {
                return m_elements.at(position);
            }
            ///* Get the size (number of components) of the variable for a given rank */
            //virtual unsigned long int size(const unsigned int rank) noexcept = 0;
        private:
            std::vector<std::shared_ptr<Symbol>> m_elements;
            bool m_row_vector;
    };
}
