/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of arithmetic classes.

   2019-02-17, Bin Gao:
   * first version
*/

#include <algorithm>
#include <utility>

#include "tSymbolic/Arithmetic.hpp"
#include "tSymbolic/Substitution.hpp"

namespace tSymbolic
{
    std::shared_ptr<Symbol> Addition::get_copy() noexcept
    {
        return std::make_shared<Addition>(std::move(*this));
    }

    std::string Addition::to_string() const noexcept
    {
        return "{addition: {augend: "+m_augend->to_string()+", addend: "+m_addend->to_string()+"}}";
    }

    bool Addition::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==type_id()) {
            /*FIXME: If pattern match works, we will remove dynamic cast */
            std::shared_ptr<Addition> other_cast = std::dynamic_pointer_cast<Addition>(other);
            return m_augend->equal_to(other_cast->m_augend) && m_addend->equal_to(other_cast->m_addend);
        }
        else {
            return false;
        }
    }

    unsigned int Addition::max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept
    {
        return std::max(m_augend->max_diff_order(var), m_addend->max_diff_order(var));
    }

    std::shared_ptr<Symbol> Addition::diffp(const VariableOrder& var) noexcept
    {
        return m_augend->diffp(var)+m_addend->diffp(var);
    }

    std::shared_ptr<Symbol> Addition::subs(const SymbolSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second;
        }
        /* Return an addition with substituted augend and addend if the
           addition was not substituted */
        return tSymbolic::subs(m_augend, substitutions)+tSymbolic::subs(m_addend, substitutions);
    }

    std::shared_ptr<Symbol> Addition::subs(const FunctionSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second(get_shared());
        }
        /* Return an addition with substituted augend and addend if the
           addition was not substituted */
        return tSymbolic::subs(m_augend, substitutions)+tSymbolic::subs(m_addend, substitutions);
    }

    std::vector<VariableOrder> Addition::get_layout(const unsigned int order) noexcept
    {
        //FIXME: layout returned by augend and addend?
        return std::vector<VariableOrder>({
            VariableOrder({std::pair<std::shared_ptr<Symbol>,unsigned int>(get_shared(), order)})
        });
    }

    std::shared_ptr<Symbol> Multiplication::get_copy() noexcept
    {
        return std::make_shared<Multiplication>(std::move(*this));
    }

    std::string Multiplication::to_string() const noexcept
    {
        return "{multiplication: {multiplier: "+m_multiplier->to_string()
            +", multiplicand: "+m_multiplicand->to_string()+"}}";
    }

    bool Multiplication::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==type_id()) {
            /*FIXME: If pattern match works, we will remove dynamic cast */
            std::shared_ptr<Multiplication> other_cast = std::dynamic_pointer_cast<Multiplication>(other);
            return m_multiplier->equal_to(other_cast->m_multiplier)
                && m_multiplicand->equal_to(other_cast->m_multiplicand);
        }
        else {
            return false;
        }
    }

    unsigned int Multiplication::max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept
    {
        return m_multiplier->max_diff_order(var)+m_multiplicand->max_diff_order(var);
    }

    //FIXME: to implement
    std::shared_ptr<Symbol> Multiplication::diffp(const VariableOrder& var) noexcept
    {
        auto multiplier_diffp = m_multiplier->diffp(var)*m_multiplicand;
        return multiplier_diffp;
    }

    std::shared_ptr<Symbol> Multiplication::subs(const SymbolSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second;
        }
        /* Return a multiplication with substituted multiplier and multiplicand
           if the multiplication was not substituted */
        return tSymbolic::subs(m_multiplier, substitutions)*tSymbolic::subs(m_multiplicand, substitutions);
    }

    std::shared_ptr<Symbol> Multiplication::subs(const FunctionSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second(get_shared());
        }
        /* Return a multiplication with substituted multiplier and multiplicand
           if the multiplication was not substituted */
        return tSymbolic::subs(m_multiplier, substitutions)*tSymbolic::subs(m_multiplicand, substitutions);
    }

    std::vector<VariableOrder> Multiplication::get_layout(const unsigned int order) noexcept
    {
        //FIXME: layout returned by multiplier and multiplicand?
        return std::vector<VariableOrder>({
            VariableOrder({std::pair<std::shared_ptr<Symbol>,unsigned int>(get_shared(), order)})
        });
    }
}
