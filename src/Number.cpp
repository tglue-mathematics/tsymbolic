/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of number class.

   2019-02-06, Bin Gao:
   * first version
*/

#include <utility>

#include "tSymbolic/Number.hpp"

namespace tSymbolic
{
    std::shared_ptr<Symbol> Number::get_copy() noexcept
    {
        return std::make_shared<Number>(std::move(*this));
    }

    std::string Number::to_string() const noexcept
    {
        if (m_val_type==typeid(int)) {
            return "{integer: "+std::to_string(m_container.integer)+"}";
        }
        else if (m_val_type==typeid(double)) {
            return "{double: "+std::to_string(m_container.real_number)+"}";
        }
        else {
            return "{complex: ("+std::to_string(m_container.complex_number.real())+','
                +std::to_string(m_container.complex_number.imag())+"i)}";
        }
    }

    bool Number::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==type_id()) {
            /*FIXME: If pattern match works, we will remove dynamic cast */
            std::shared_ptr<Number> other_cast = std::dynamic_pointer_cast<Number>(other);
            if (m_val_type==typeid(int)) {
                if (other_cast->m_val_type==typeid(int)) {
                    return other_cast->m_container.integer==m_container.integer;
                }
                else if (other_cast->m_val_type==typeid(double)) {
                    return other_cast->m_container.real_number==m_container.integer;
                }
                else {
                    return other_cast->m_container.complex_number==std::complex<double>(m_container.integer, 0);
                }
            }
            else if (m_val_type==typeid(double)) {
                if (other_cast->m_val_type==typeid(int)) {
                    return other_cast->m_container.integer==m_container.real_number;
                }
                else if (other_cast->m_val_type==typeid(double)) {
                    return other_cast->m_container.real_number==m_container.real_number;
                }
                else {
                    return other_cast->m_container.complex_number==m_container.real_number;
                }
            }
            else {
                if (other_cast->m_val_type==typeid(int)) {
                    return std::complex<double>(other_cast->m_container.integer, 0)==m_container.complex_number;
                }
                else if (other_cast->m_val_type==typeid(double)) {
                    return other_cast->m_container.real_number==m_container.complex_number;
                }
                else {
                    return other_cast->m_container.complex_number==m_container.complex_number;
                }
            }
        }
        else {
            return false;
        }
    }

    unsigned int Number::max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept
    {
        return 0;
    }

    std::shared_ptr<Symbol> Number::diffp(const VariableOrder& var) noexcept
    {
        return std::make_shared<Number>(Number(0));
    }
}
