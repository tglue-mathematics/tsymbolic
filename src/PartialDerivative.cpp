/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of partial derivative class.

   2019-09-30, Bin Gao:
   * first version
*/

#include <utility>

#include "tSymbolic/PartialDerivative.hpp"
#include "tSymbolic/VariableOrder.hpp"
#include "tSymbolic/Substitution.hpp"

namespace tSymbolic
{
    std::shared_ptr<Symbol> PartialDerivative::get_copy() noexcept
    {
        return std::make_shared<PartialDerivative>(std::move(*this));
    }

    std::string PartialDerivative::to_string() const noexcept
    {
        return "{derivative: {function: "+m_fun->to_string()+", variables: "+tSymbolic::stringify(m_var)+"}}";
    }

    bool PartialDerivative::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==type_id()) {
            /*FIXME: If pattern match works, we will remove dynamic cast */
            std::shared_ptr<PartialDerivative> other_cast = std::dynamic_pointer_cast<PartialDerivative>(other);
            if (m_fun->equal_to(other_cast->m_fun)) {
                if (m_var.size()==other_cast->m_var.size()) {
                    for (auto each_var=m_var.cbegin(),other_var=other_cast->m_var.cbegin();
                         each_var!=m_var.cend();
                         ++each_var,++other_var) {
                        if (!(each_var->first->equal_to(other_var->first) && each_var->second==other_var->second)) return false;
                    }
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    unsigned int PartialDerivative::max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept
    {
        auto order_fun = m_fun->max_diff_order(var);
        for (auto const& each_var: m_var) {
            if (each_var.first->equal_to(var)) {
                order_fun -= each_var.second;
                break;
            }
        }
        return order_fun;
    }

    std::shared_ptr<Symbol> PartialDerivative::diffp(const VariableOrder& var) noexcept
    {
        //return std::make_shared<PartialDerivative>(PartialDerivative(m_fun, tSymbolic::simplify(m_var, var)));
        return m_fun->diffp(tSymbolic::simplify(m_var, var));
    }

    std::shared_ptr<Symbol> PartialDerivative::subs(const SymbolSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second;
        }
        /* If the partial derivative itself was not substituted, we try to
           substitute its function and/or variables */
        auto new_fun = tSymbolic::subs(m_fun, substitutions);
        VariableOrder new_var;
        new_var.reserve(m_var.size());
        for (auto const& each_var: m_var) {
            new_var.push_back(
                std::pair<std::shared_ptr<Symbol>,unsigned int>(
                    tSymbolic::subs(each_var.first, substitutions),
                    each_var.second
                )
            );
        }
        return new_fun->diffp(new_var);
    }

    std::shared_ptr<Symbol> PartialDerivative::subs(const FunctionSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second(get_shared());
        }
        /* If the function matches a substitution, we send the whole
           PartialDerivative to std::function */
        for (auto const& each_sub: substitutions) {
            if (m_fun->equal_to(each_sub.first)) return each_sub.second(get_shared());
        }
        /* If neither PartialDerivative nor its function was not substituted,
           we then invoke subs() of the function and also try to substitute
           variables */
        auto new_fun = tSymbolic::subs(m_fun, substitutions);
        VariableOrder new_var;
        new_var.reserve(m_var.size());
        for (auto const& each_var: m_var) {
            new_var.push_back(
                std::pair<std::shared_ptr<Symbol>,unsigned int>(
                    tSymbolic::subs(each_var.first, substitutions),
                    each_var.second
                )
            );
        }
        return new_fun->diffp(new_var);
    }

    //FIXME
    std::vector<VariableOrder> PartialDerivative::get_layout(const unsigned int order) noexcept
    {
        return std::vector<VariableOrder>({
            VariableOrder({std::pair<std::shared_ptr<Symbol>,unsigned int>(get_shared(), order)})
        });
    }
}
