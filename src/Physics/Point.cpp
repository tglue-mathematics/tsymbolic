/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of point class.

   2020-05-21, Bin Gao:
   * first version
*/

#include <utility>

#include "tGlueCore/Stringify.hpp"

#include "tSymbolic/Physics/Point.hpp"

namespace tSymbolic
{
    std::shared_ptr<Symbol> Point::get_copy() noexcept
    {
        return std::make_shared<Point>(std::move(*this));
    }

    std::string Point::to_string() const noexcept
    {
        return "{point: "+tGlueCore::stringify(this)+", index: "+std::to_string(m_index)+"}";
    }

    bool Point::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==type_id()) {
            /*FIXME: If pattern match works, we will remove dynamic cast */
            std::shared_ptr<Point> other_cast = std::dynamic_pointer_cast<Point>(other);
            return m_index==other_cast->m_index;
        }
        else {
            return false;
        }
    }

    //bool Point::equal_to(const std::shared_ptr<Point>& other) const noexcept
    //{
    //    return m_index==other->m_index;
    //}
}
