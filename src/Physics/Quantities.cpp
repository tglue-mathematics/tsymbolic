/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements physical quantities.

   2020-05-15, Bin Gao:
   * first version
*/

#include <utility>

#include "tGlueCore/Stringify.hpp"

#include "tSymbolic/Physics/Quantities.hpp"

namespace tSymbolic
{
    std::shared_ptr<Symbol> MagFluxDensity::get_copy() noexcept
    {
        return std::make_shared<MagFluxDensity>(std::move(*this));
    }

    std::string MagFluxDensity::to_string() const noexcept
    {
        return "{magnetic-flux-density: "+tGlueCore::stringify(this)
            +", frequency: ("+std::to_string(m_frequency.real())+','+std::to_string(m_frequency.imag())+"i)}";
    }

    bool MagFluxDensity::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==type_id()) {
            /*FIXME: If pattern match works, we will remove dynamic cast */
            std::shared_ptr<MagFluxDensity> other_cast = std::dynamic_pointer_cast<MagFluxDensity>(other);
            return m_frequency==other_cast->m_frequency;
        }
        else {
            return false;
        }
    }

    std::shared_ptr<Symbol> TotalRotMomentum::get_copy() noexcept
    {
        return std::make_shared<TotalRotMomentum>(std::move(*this));
    }

    std::string TotalRotMomentum::to_string() const noexcept
    {
        return "{total-rotational-angular-momentum: "+tGlueCore::stringify(this)
            +", frequency: ("+std::to_string(m_frequency.real())+','+std::to_string(m_frequency.imag())+"i)}";
    }

    bool TotalRotMomentum::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==type_id()) {
            /*FIXME: If pattern match works, we will remove dynamic cast */
            std::shared_ptr<TotalRotMomentum> other_cast = std::dynamic_pointer_cast<TotalRotMomentum>(other);
            return m_frequency==other_cast->m_frequency;
        }
        else {
            return false;
        }
    }
}
