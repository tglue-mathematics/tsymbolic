/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of power product class.

   2020-05-10, Bin Gao:
   * first version
*/

#include "tSymbolic/PowerProduct.hpp"

namespace tSymbolic
{
    std::string PowerProduct::to_string() const noexcept
    {
        return "{power-product: "+stringify(m_var_orders)+"}";
    }

    bool PowerProduct::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==typeid(*this)) {
            if (m_var_orders.size()==other->m_var_orders.size()) {
                for (auto each_var=m_var_orders.cbegin(),other_var=other->m_var_orders.cbegin();
                     each_var!=m_var_orders.cend();
                     ++each_var,++other_var) {
                    if (!(each_var->first->equal_to(other_var->first) && each_var->second==other_var->second)) return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    std::shared_ptr<Symbol> PowerProduct::diffp(const PowerProductOrder& var) const noexcept
    {
        if (var.size()==1) {
            /* First order */
            if (var[0].second==1) {
                if (equal_to(var[0].first)) {
                    return Number::create(1);
                }
                else {
                    return Number::create(0);
                }
            }
            /* Higher order */
            else {
                return Number::create(0);
            }
        }
        else {
            return Number::create(0);
        }
    }

    std::shared_ptr<Symbol> PowerProduct::subs(const SymbolSubstitution& substitutions) const noexcept
    {
        for (auto const& each_var: m_var_orders) {
            auto var_subs = tSymbolic::subs(each_var, substitutions);
        }
        return PowerProduct::create(m_name);
    }

    std::shared_ptr<Symbol> PowerProduct::subs(const FunctionSubstitution& substitutions) const noexcept
    {
    }

    std::type_index PowerProduct::type_id() const noexcept
    {
        return typeid(*this);
    }
}
