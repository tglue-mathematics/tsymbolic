/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of symbol class.

   2019-02-06, Bin Gao:
   * first version
*/

#include "tGlueCore/Stringify.hpp"

#include "tSymbolic/Symbol.hpp"
#include "tSymbolic/Number.hpp"
#include "tSymbolic/PartialDerivative.hpp"

namespace tSymbolic
{
    std::shared_ptr<Symbol> Symbol::get_shared() noexcept
    {
        return shared_from_this();
    }

    std::shared_ptr<Symbol> Symbol::get_copy() noexcept
    {
        return std::make_shared<Symbol>(std::move(*this));
    }

    std::string Symbol::to_string() const noexcept
    {
        return "{symbol: "+tGlueCore::stringify(this)+"}";
    }

    bool Symbol::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        return this==other.get();
    }

    unsigned int Symbol::max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept
    {
        return equal_to(var) ? 1 : 0;
    }

    std::shared_ptr<Symbol> Symbol::diffp(const VariableOrder& var) noexcept
    {
        bool zero_derivatives = false;
        for (auto const& each_var: var) {
            if (max_diff_order(each_var.first)<each_var.second) {
                zero_derivatives = true;
                break;
            }
        }
        if (zero_derivatives) {
            return std::make_shared<Number>(Number(0));
        }
        else {
            return std::make_shared<PartialDerivative>(get_shared(), var);
        }
    }

    std::shared_ptr<Symbol> Symbol::subs(const SymbolSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second;
        }
        /* Return the symbol itself if no substitution made */
        return get_copy();
    }

    std::shared_ptr<Symbol> Symbol::subs(const FunctionSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second(get_shared());
        }
        /* Return the symbol itself if no substitution made */
        return get_copy();
    }

    std::vector<VariableOrder> Symbol::get_layout(const unsigned int order) noexcept
    {
        if (order==0) return std::vector<VariableOrder>();
        /* We treat the symbol as scalar */
        return std::vector<VariableOrder>({
            VariableOrder({std::pair<std::shared_ptr<Symbol>,unsigned int>(shared_from_this(), order)})
        });
    }

    std::type_index Symbol::type_id() const noexcept
    {
        return typeid(*this);
    }
}
