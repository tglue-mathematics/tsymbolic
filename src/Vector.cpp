/* tSymbolic: library of symbolic computations
   Copyright 2015-2020 Bin Gao

   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.

   This file implements functions of vector class.

   2020-04-22, Bin Gao:
   * first version
*/

#include <limits>
#include <utility>

#include "tGlueCore/Logger.hpp"
#include "tGlueCore/Stringify.hpp"

#include "tSymbolic/Settings.hpp"
#include "tSymbolic/Vector.hpp"
#include "tSymbolic/Combinatorics/Numbers.hpp"
#include "tSymbolic/Combinatorics/TwelvefoldWay.hpp"
#include "tSymbolic/VariableOrder.hpp"
#include "tSymbolic/Substitution.hpp"

namespace tSymbolic
{
    std::shared_ptr<Symbol> Vector::get_copy() noexcept
    {
        return std::make_shared<Vector>(std::move(*this));
    }

    std::string Vector::to_string() const noexcept
    {
        if (m_row_vector) {
            return "{row-vector: "+tGlueCore::stringify(m_elements.cbegin(), m_elements.cend())+"}";
        }
        else {
            return "{column-vector: "+tGlueCore::stringify(m_elements.cbegin(), m_elements.cend())+"}";
        }
    }

    bool Vector::equal_to(const std::shared_ptr<Symbol>& other) const noexcept
    {
        if (other->type_id()==type_id()) {
            /*FIXME: If pattern match works, we will remove dynamic cast */
            std::shared_ptr<Vector> other_cast = std::dynamic_pointer_cast<Vector>(other);
            if (is_same_structure(other_cast)) {
                auto each_element = m_elements.cbegin();
                auto other_element = other_cast->m_elements.cbegin();
                for (; each_element!=m_elements.cend(); ++each_element,++other_element) {
                    if (!(*each_element)->equal_to(*other_element)) return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    unsigned int Vector::max_diff_order(const std::shared_ptr<Symbol>& var) const noexcept
    {
        unsigned int max_order = 0;
        for (auto const each_element: m_elements) {
            auto order_element = each_element->max_diff_order(var);
            if (max_order<order_element) max_order = order_element;
        }
        return max_order;
    }

    std::shared_ptr<Symbol> Vector::diffp(const VariableOrder& var) noexcept
    {
        return std::make_shared<Vector>(
            Vector(tSymbolic::diffp(m_elements.begin(), m_elements.end(), var), m_row_vector)
        );
    }

    std::shared_ptr<Symbol> Vector::subs(const SymbolSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second;
        }
        /* Return a vector with substituted elements if itself was not
           substituted */
        return std::make_shared<Vector>(
            Vector(tSymbolic::subs(m_elements.begin(), m_elements.end(), substitutions), m_row_vector)
        );
    }

    std::shared_ptr<Symbol> Vector::subs(const FunctionSubstitution& substitutions) noexcept
    {
        for (auto const& each_sub: substitutions) {
            if (equal_to(each_sub.first)) return each_sub.second(get_shared());
        }
        /* Return a vector with substituted elements if itself was not
           substituted */
        return std::make_shared<Vector>(
            Vector(tSymbolic::subs(m_elements.begin(), m_elements.end(), substitutions), m_row_vector)
        );
    }

    /* The components are commutative in the following implementation */
    std::vector<VariableOrder> Vector::get_layout(const unsigned int order) noexcept
    {
        if (order==0) return std::vector<VariableOrder>();
        std::vector<VariableOrder> comp_layout;
        /* Size of layout is \binom{order+number_of_elements-1}{order} */
        auto size_layout = binom_coefficient<unsigned int>(order+m_elements.size()-1, order);
        if (size_layout==std::numeric_limits<unsigned int>::max()) {
            Settings::logger->write(tGlueCore::MessageType::Error,
                                    "Vector::get_layout() could not handle the layout with required order");
            return comp_layout;
        }
        comp_layout.reserve(size_layout);
        /* Generate layout from multisets with \var(order) elements */
        tSymbolic::for_each_multiset(order, m_elements.size(), [&](const std::vector<unsigned int>& element_orders) {
            /* Pick up elements with non-zero orders */
            VariableOrder nonzero_elements;
            auto each_element = m_elements.cbegin();
            for (auto each_order=element_orders.cbegin();
                 each_order!=element_orders.cend();
                 ++each_order,++each_element) {
                if (*each_order>0) {
                    nonzero_elements.push_back(
                        std::pair<std::shared_ptr<Symbol>,unsigned int>(*each_element, *each_order)
                    );
                }
            }
            comp_layout.push_back(nonzero_elements);
        });
        return comp_layout;
    }
}
